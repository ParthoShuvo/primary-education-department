package com.nationalappsbd.primaryeducationdepartment.DataBaseTables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;
import java.util.List;

/**
 * Created by shuvojit on 5/19/15.
 */

@Table(name = "working_procedure")
public class WorkingProcedure extends Model implements Serializable{

    @Column(name = "details")
    private String details;

    public WorkingProcedure() {
        super();
    }

    public WorkingProcedure(String details) {
        super();
        this.details = details;
        this.save();
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public static List<WorkingProcedure> getWorkingProcedureList() {
        List<WorkingProcedure> workingProcedureList = null;
        workingProcedureList = new Select()
                .from(WorkingProcedure.class)
                .execute();
        return workingProcedureList;
    }
}
