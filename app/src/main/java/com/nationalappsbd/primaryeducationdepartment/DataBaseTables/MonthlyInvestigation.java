package com.nationalappsbd.primaryeducationdepartment.DataBaseTables;

import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by shuvojit on 5/25/15.
 */

@Table(name = "monthly_investigation")
public class MonthlyInvestigation extends ProjectPedp3 {

    public MonthlyInvestigation() {
        super();
    }

    public MonthlyInvestigation(String rulsName, String urlLink) {
        super(rulsName, urlLink);
        this.save();
    }

    public static List<MonthlyInvestigation> getMonthlyInvestigationList()
    {
        List<MonthlyInvestigation> monthlyInvestigationList = null;
        monthlyInvestigationList = new Select()
                .from(MonthlyInvestigation.class)
                .execute();
        return monthlyInvestigationList;
    }
}
