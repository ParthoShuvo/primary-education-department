package com.nationalappsbd.primaryeducationdepartment.DataBaseTables;

import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;

/**
 * Created by shuvojit on 5/19/15.
 */
@Table(name = "future_plan")
public class FuturePlan extends History implements Serializable{

    public FuturePlan() {
        super();
    }

    public FuturePlan(String details) {
        super(details);
    }

    public static FuturePlan getFuturePlan() {
        FuturePlan futurePlan = null;
        futurePlan = new Select()
                .from(FuturePlan.class)
                .executeSingle();
        return futurePlan;
    }
}
