package com.nationalappsbd.primaryeducationdepartment.DataBaseTables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by shuvojit on 5/25/15.
 */

@Table(name = "project_pedp_3")
public class ProjectPedp3 extends Model {

    @Column(name = "rules")
    private String rules;

    @Column(name = "url_link")
    private String urlLink;

    public ProjectPedp3() {
        super();
    }

    public ProjectPedp3(String rules, String urlLink) {
        this.rules = rules;
        this.urlLink = urlLink;
    }

    public static List<ProjectPedp3> getProjectPedp3List() {
        List<ProjectPedp3> projectPedp3List = null;
        projectPedp3List = new Select()
                .from(ProjectPedp3.class)
                .execute();
        return projectPedp3List;
    }

    public String getRules() {
        return rules;
    }

    public void setRules(String rules) {
        this.rules = rules;
    }

    public String getUrlLink() {
        return urlLink;
    }

    public void setUrlLink(String urlLink) {
        this.urlLink = urlLink;
    }
}
