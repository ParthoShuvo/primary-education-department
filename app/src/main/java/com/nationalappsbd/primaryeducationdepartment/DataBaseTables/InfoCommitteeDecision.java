package com.nationalappsbd.primaryeducationdepartment.DataBaseTables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;

/**
 * Created by shuvojit on 5/23/15.
 */

@Table(name = "info_committee_dicision")
public class InfoCommitteeDecision extends Model implements Serializable {

    @Column(name = "url_link")
    private String urlLink;

    public InfoCommitteeDecision() {
        super();
    }

    public InfoCommitteeDecision(String urlLink) {
        super();
        this.urlLink = urlLink;
        this.save();
    }

    public String getUrlLink() {
        return urlLink;
    }

    public void setUrlLink(String urlLink) {
        this.urlLink = urlLink;
    }

    public static InfoCommitteeDecision getInfoCommitteeDecision()
    {
        InfoCommitteeDecision infoCommitteeDecision = null;
        infoCommitteeDecision = new Select()
                .from(InfoCommitteeDecision.class)
                .executeSingle();
        return infoCommitteeDecision;
    }
}
