package com.nationalappsbd.primaryeducationdepartment.DataBaseTables;

import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by shuvojit on 5/25/15.
 */

@Table(name = "publications")
public class Publications extends ProjectPedp3 {

    public Publications() {
        super();
    }

    public Publications(String rulsName, String urlLink) {
        super(rulsName, urlLink);
        this.save();
    }

    public static List<Publications> getPublicationsList()
    {
        List<Publications> publicationsList = null;
        publicationsList = new Select()
                .from(Publications.class)
                .execute();
        return publicationsList;
    }
}
