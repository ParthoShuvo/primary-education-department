package com.nationalappsbd.primaryeducationdepartment.DataBaseTables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;

/**
 * Created by shuvojit on 5/19/15.
 */

@Table(name = "history")
public class History extends Model implements Serializable{

    @Column(name = "details")
    private String details;

    public History() {
        super();
    }

    public History(String details) {
        super();
        this.details = details;
        this.save();
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public static History getHistory() {
        History history = null;
        history = new Select()
                .from(History.class)
                .executeSingle();
        return history;
    }
}
