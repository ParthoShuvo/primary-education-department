package com.nationalappsbd.primaryeducationdepartment.DataBaseTables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;
import java.util.List;

/**
 * Created by shuvojit on 5/19/15.
 */

@Table(name = "e_primary_system")
public class E_PrimarySystem extends Model implements Serializable{

    @Column(name = "name")
    private String name;

    @Column(name = "url_link")
    private String urlLink;

    public E_PrimarySystem() {
        super();
    }

    public E_PrimarySystem(String name, String urlLink) {
        super();
        this.name = name;
        this.urlLink = urlLink;
        this.save();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrlLink() {
        return urlLink;
    }

    public void setUrlLink(String urlLink) {
        this.urlLink = urlLink;
    }
    public static List<E_PrimarySystem> getE_primarySystemList()
    {
        List<E_PrimarySystem> e_primarySystemList = null;
        e_primarySystemList = new Select()
                .from(E_PrimarySystem.class)
                .execute();
        return e_primarySystemList;
    }
}
