package com.nationalappsbd.primaryeducationdepartment.DataBaseTables;

import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;
import java.util.List;

/**
 * Created by shuvojit on 5/23/15.
 */

@Table(name = "givense_redress_system")
public class GivenseRedressSystem extends InformationRights implements Serializable {

    public GivenseRedressSystem() {
        super();
    }

    public GivenseRedressSystem(String urlLink, String info) {
        super(urlLink, info);
    }

    public static List<GivenseRedressSystem> getGivenseRedressSystemList() {
        List<GivenseRedressSystem> givenseRedressSystemList = null;
        givenseRedressSystemList = new Select()
                .from(GivenseRedressSystem.class)
                .execute();
        return givenseRedressSystemList;
    }
}
