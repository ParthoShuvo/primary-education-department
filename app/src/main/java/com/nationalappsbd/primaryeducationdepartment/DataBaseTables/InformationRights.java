package com.nationalappsbd.primaryeducationdepartment.DataBaseTables;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;
import java.util.List;

/**
 * Created by shuvojit on 5/23/15.
 */

@Table(name = "information_rights")
public class InformationRights extends InfoCommitteeDecision implements Serializable {

    @Column(name = "info")
    private String info;



    public InformationRights() {
        super();
    }

    public InformationRights(String urlLink, String info) {
        super(urlLink);
        this.info = info;
        this.save();
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public static List<InformationRights> getInformationRightsList() {
        List<InformationRights> informationRightsList = null;
        informationRightsList = new Select().from(InformationRights.class).execute();
        return informationRightsList;
    }
}
