package com.nationalappsbd.primaryeducationdepartment.DataBaseTables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;

/**
 * Created by shuvojit on 5/19/15.
 */

@Table(name = "primary_edu_dept_info")
public class PrimaryEducationDepartmentInfo extends Model implements Serializable{

    @Column(name = "address")
    private String address;

    @Column(name = "phone")
    private String phone;

    @Column(name = "fax")
    private String fax;

    @Column(name = "email")
    private String email;

    @Column(name = "latitude")
    private double latitude;

    @Column(name = "longitude")
    private double longitude;

    @Column(name = "image_link")
    private String imageLink;

    public PrimaryEducationDepartmentInfo() {
        super();
    }

    public PrimaryEducationDepartmentInfo(String address, String phone,
                                          String fax, String email, double latitude,
                                          double longitude, String imageLink) {
        super();
        this.address = address;
        this.phone = phone;
        this.fax = fax;
        this.email = email;
        this.latitude = latitude;
        this.longitude = longitude;
        this.imageLink = imageLink;
        this.save();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public static PrimaryEducationDepartmentInfo getPrimaryEducationDepartmentInfo() {
        PrimaryEducationDepartmentInfo primaryEducationDepartmentInfo = null;
        primaryEducationDepartmentInfo = new Select()
                .from(PrimaryEducationDepartmentInfo.class)
                .executeSingle();
        return primaryEducationDepartmentInfo;
    }
}
