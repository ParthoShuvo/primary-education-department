package com.nationalappsbd.primaryeducationdepartment.DataBaseTables;

import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by shuvojit on 5/25/15.
 */

@Table(name = "forms")
public class Forms extends ProjectPedp3 {

    public Forms() {
        super();
    }

    public Forms(String rulsName, String urlLink) {
        super(rulsName, urlLink);
        this.save();
    }

    public static List<Forms> getFormsList() {
        List<Forms> formsList = null;
        formsList = new Select()
                .from(Forms.class)
                .execute();
        return formsList;
    }

}
