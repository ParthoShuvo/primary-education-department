package com.nationalappsbd.primaryeducationdepartment.DataBaseTables;

import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by shuvojit on 5/25/15.
 */

@Table(name = "school_result_statictics")
public class SchoolResultStatictics extends ProjectPedp3 {

    public SchoolResultStatictics() {
        super();
    }

    public SchoolResultStatictics(String rulsName, String urlLink) {
        super(rulsName, urlLink);
        this.save();
    }

    public static List<SchoolResultStatictics> getSchoolResultStaticticsList() {
        List<SchoolResultStatictics> schoolResultStaticticsList = null;
        schoolResultStaticticsList = new Select()
                .from(SchoolResultStatictics.class)
                .execute();
        return schoolResultStaticticsList;

    }
}
