package com.nationalappsbd.primaryeducationdepartment.DataBaseTables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;
import java.util.List;

/**
 * Created by shuvojit on 5/19/15.
 */

@Table(name = "working_period_director_general")
public class WorkingPeriodDirectorGeneral extends Model implements Serializable {

    @Column(name = "name")
    private String name;

    @Column(name = "from_date")
    private String fromDate;

    @Column(name = "to_date")
    private String toDate;

    public WorkingPeriodDirectorGeneral() {
        super();
    }

    public WorkingPeriodDirectorGeneral(String name, String fromDate, String toDate) {
        super();
        this.name = name;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.save();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public static List<WorkingPeriodDirectorGeneral> getWorkingPeriodDtrectorGeneralList()
    {
        List<WorkingPeriodDirectorGeneral> workingPeriodDirectorGeneralList = null;
        workingPeriodDirectorGeneralList = new Select()
                .from(WorkingPeriodDirectorGeneral.class)
                .execute();
        return workingPeriodDirectorGeneralList;
    }
}
