package com.nationalappsbd.primaryeducationdepartment.DataBaseTables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;
import java.util.List;

/**
 * Created by shuvojit on 6/5/15.
 */
@Table(name = "officers_name_list")
public class OfficersNameList extends Model implements Serializable {


    @Column(name = "name")
    private String name;

    @Column(name = "designation")
    private String designation;

    @Column(name = "email")
    private String email;

    @Column(name = "phone")
    private String phone;

    @Column(name = "image_link")
    private String imageLink;

    @Column(name = "mobile")
    private String mobile;

    @Column(name = "office")
    private String Office;

    @Column(name = "fax")
    private String fax;

    public OfficersNameList() {
        super();
    }

    public OfficersNameList(String name, String designation, String email,
                            String phone, String imageLink, String mobile,
                            String office, String fax) {
        super();
        this.name = name;
        this.designation = designation;
        this.email = email;
        this.phone = phone;
        this.imageLink = imageLink;
        this.mobile = mobile;
        this.Office = office;
        this.fax = fax;
        this.save();
    }

    public static List<OfficersNameList> getOfficersNameLists() {
        List<OfficersNameList> OfficersNameLists = null;
        OfficersNameLists = new Select().from(OfficersNameList.class).execute();
        return OfficersNameLists;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getOffice() {
        return Office;
    }

    public void setOffice(String office) {
        Office = office;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }
}
