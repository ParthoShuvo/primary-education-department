package com.nationalappsbd.primaryeducationdepartment.DataBaseTables;

import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by shuvojit on 5/24/15.
 */

@Table(name = "advertisement")
public class Advertisement extends Rules {

    public Advertisement() {
        super();
    }

    public Advertisement(String rulsName, String urlLink) {
        super(rulsName, urlLink);
        this.save();
    }

    public static List<Advertisement> getAdvertisementList() {
        List<Advertisement> advertisementList = null;
        advertisementList = new Select()
                .from(Advertisement.class)
                .execute();
        return advertisementList;
    }

}
