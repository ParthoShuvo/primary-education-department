package com.nationalappsbd.primaryeducationdepartment.fragments;

/**
 * Created by shuvojit on 5/8/15.
 */

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.PrimaryEducationDepartmentInfo;
import com.nationalappsbd.primaryeducationdepartment.R;
import com.nationalappsbd.primaryeducationdepartment.adapters.googleMapInfoWindowAdapters.InfoWindowAdapterForEachSpot;
import com.nationalappsbd.primaryeducationdepartment.dialogs.UserNotifiedDialog;
import com.nationalappsbd.primaryeducationdepartment.interfaces.GoogleMapClient;
import com.nationalappsbd.primaryeducationdepartment.interfaces.Initializer;


public class PrimaryEducationDepartmentInGoogleMapFragment extends Fragment
        implements Initializer, GoogleMapClient {


    /**
     * Created by Shuvojit Saha Shuvo on 1/26/2015.
     */

    private MapFragment mapFragment;
    private android.app.FragmentManager fragmentManager;
    private GoogleMap googleMap;
    private Bundle bundle;
    private double googleMapCameraZoomLatitude;
    private double googleMapCameraZoomLongtitude;
    private Context context;
    private View fragmentView;
    private UiSettings googleMapUiSettings;
    private float cameraZoom;
    private PrimaryEducationDepartmentInfo primaryEducationDepartmentInfo;
    private TextView txtAddress;
    private TextView txtPhone;


    public PrimaryEducationDepartmentInGoogleMapFragment() {

    }


    public static PrimaryEducationDepartmentInGoogleMapFragment newInstance
            (PrimaryEducationDepartmentInfo primaryEducationDepartmentInfo,
             double googleMapCameraZoomLatitude,
             double googleMapCameraZoomLongtitude,
             float cameraZoom) {
        PrimaryEducationDepartmentInGoogleMapFragment primaryEducationDepartmentInGoogleMapFragment =
                new PrimaryEducationDepartmentInGoogleMapFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("Primary Education Department", primaryEducationDepartmentInfo);
        bundle.putDouble("GoogleMapCameraZoomLatitude", googleMapCameraZoomLatitude);
        bundle.putDouble("GoogleMapCameraZoomLongtitude", googleMapCameraZoomLongtitude);
        bundle.putFloat("CameraZoom", cameraZoom);
        primaryEducationDepartmentInGoogleMapFragment.setArguments(bundle);
        return primaryEducationDepartmentInGoogleMapFragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bundle = getArguments();
        primaryEducationDepartmentInfo = (PrimaryEducationDepartmentInfo) bundle
                .getSerializable("Primary Education Department");
        googleMapCameraZoomLatitude = bundle.getDouble("GoogleMapCameraZoomLatitude");
        googleMapCameraZoomLongtitude = bundle.getDouble("GoogleMapCameraZoomLongtitude");
        cameraZoom = bundle.getFloat("CameraZoom");
        context = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (fragmentView == null) {
            fragmentView = inflater
                    .inflate(R.layout.primary_education_department_in_google_map, container,
                            false);
            initialize();
            setGoogleMapZoom(googleMapCameraZoomLatitude, googleMapCameraZoomLongtitude,
                    cameraZoom);
            addMarkerOnGoogleMap();
            setGoogleMapUiSettings();
            googleMap.setInfoWindowAdapter(new InfoWindowAdapterForEachSpot(context));
        }
        return fragmentView;
    }

    @Override
    public void initialize() {
        if (fragmentView != null) {
            txtAddress = (TextView) fragmentView.findViewById(R.id.txtAddress);
            txtPhone = (TextView) fragmentView.findViewById(R.id.txtPhone);
            txtAddress.setText(primaryEducationDepartmentInfo.getAddress());
            txtPhone.setText("+8802-8057877");
            fragmentManager = getActivity().getFragmentManager();
            mapFragment = (MapFragment) fragmentManager
                    .findFragmentById(R.id.googleMap);
            googleMap = mapFragment.getMap();
            googleMapUiSettings = googleMap.getUiSettings();
            txtPhone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String text = txtPhone.getText().toString();
                    if (text != null && !text.equals("")) {
                        String msg = "প্রাথমিক শিক্ষা অধিদপ্তরে কল করার জন্য আপনার কল চার্জ প্রযোজ্য হবে ";
                        UserNotifiedDialog userNotifiedDialog = new
                                UserNotifiedDialog(context, "Calling Alert", msg, text);
                        userNotifiedDialog.showDialog();
                    }
                }
            });
            Log.e(getClass().getName(), "Google Map has been added");
        }
    }

    @Override
    public void setGoogleMapZoom(double latitude, double longtitude, float cameraZoom) {
        if (googleMap != null) {
            LatLng Bangladesh = new LatLng(latitude, longtitude);
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Bangladesh, cameraZoom));

        }
    }

    @Override
    public void setMarkerColor(Marker marker) {
        if (marker != null) {
            marker.setIcon
                    (BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW));
        }
    }

    @Override
    public void addMarkerOnGoogleMap() {
        if (primaryEducationDepartmentInfo != null && googleMap != null) {
            LatLng latLong = null;
            MarkerOptions markerOptions = null;
            Marker mapMarker = null;
            double latitude = primaryEducationDepartmentInfo.getLatitude();
            double longtitude = primaryEducationDepartmentInfo.getLongitude();
            String snippet = context.getResources().getString(R.string.app_name);
            latLong = new LatLng(latitude, longtitude);
            markerOptions = new MarkerOptions()
                    .position(latLong)
                    .title(snippet);
            mapMarker = googleMap.addMarker(markerOptions);
            setMarkerColor(mapMarker);
            mapMarker.showInfoWindow();

        }
    }


    public void setGoogleMapUiSettings() {
        if (googleMapUiSettings != null) {
            googleMapUiSettings.setScrollGesturesEnabled(false);
            googleMapUiSettings.setZoomControlsEnabled(true);
            googleMapUiSettings.setMapToolbarEnabled(true);
            googleMapUiSettings.setCompassEnabled(true);

        }
    }


    @Override
    public void onDestroy() {
        try {
            fragmentManager.beginTransaction().remove(mapFragment).commit();
            //remove(mapFragment).commit();
            googleMap = null;
            mapFragment = null;
            fragmentManager = null;
            fragmentView = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e(getClass().getName(), "Destroy map");
        super.onDestroy();
    }


}

