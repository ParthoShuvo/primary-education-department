package com.nationalappsbd.primaryeducationdepartment.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.Advertisement;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.CentralEServices;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.DirectorGeneral;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.DistrictOffice;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.DivisionOffice;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.E_PrimarySystem;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.Forms;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.FuturePlan;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.GivenseRedressSystem;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.History;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.ImportantLink;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.InfoProvidingOfficer;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.InformationRights;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.InternalServices;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.MadrashaResultStatictics;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.MonthlyInvestigation;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.NewsUpdate;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.Notice;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.OfficersNameList;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.PrimaryEducationDepartmentInfo;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.ProjectPedp2;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.ProjectPedp3;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.PtiOffice;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.Publications;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.Rules;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.SchoolResultStatictics;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.TenderNotice;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.WorkingPeriodDirectorGeneral;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.WorkingProcedure;
import com.nationalappsbd.primaryeducationdepartment.R;
import com.nationalappsbd.primaryeducationdepartment.activities.FragmentActivity;
import com.nationalappsbd.primaryeducationdepartment.adapters.listViewAdapters.PrimaryEduDeptInfoListViewAdapter;
import com.nationalappsbd.primaryeducationdepartment.dialogs.UserNotifiedDialog;
import com.nationalappsbd.primaryeducationdepartment.infos.ConnectivityInfo;
import com.nationalappsbd.primaryeducationdepartment.webServices.WebBrowser;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by shuvojit on 5/19/15.
 */
public class PrimaryEducationDepartmentNecessaryInfoFragment extends Fragment {

    private Context context;
    private LayoutInflater layoutInflater;
    private int FRAGMENT_SHOW_TYPE;
    private String FRAGMENT_NAME;
    private ListView listView;
    private List<NewsUpdate> newsUpdateList;
    private NewsUpdate newsUpdate;

    public PrimaryEducationDepartmentNecessaryInfoFragment() {

    }

    public static PrimaryEducationDepartmentNecessaryInfoFragment getNewInstance
            (final int FRAGMENT_SHOW_TYPE, final String Fragment_Name) {
        Bundle bundle = new Bundle();
        bundle.putInt("FRAGMENT_SHOW_TYPE", FRAGMENT_SHOW_TYPE);
        bundle.putString("FRAGMENT_NAME", Fragment_Name);
        PrimaryEducationDepartmentNecessaryInfoFragment
                primaryEducationDepartmentNecessaryInfoFragment = new
                PrimaryEducationDepartmentNecessaryInfoFragment();
        primaryEducationDepartmentNecessaryInfoFragment.setArguments(bundle);
        return primaryEducationDepartmentNecessaryInfoFragment;
    }

    public static PrimaryEducationDepartmentNecessaryInfoFragment getNewInstance
            (final int FRAGMENT_SHOW_TYPE, final String Fragment_Name,
             final NewsUpdate newsUpdate) {
        Bundle bundle = new Bundle();
        bundle.putInt("FRAGMENT_SHOW_TYPE", FRAGMENT_SHOW_TYPE);
        bundle.putString("FRAGMENT_NAME", Fragment_Name);
        bundle.putSerializable("News Details", newsUpdate);
        PrimaryEducationDepartmentNecessaryInfoFragment
                primaryEducationDepartmentNecessaryInfoFragment = new
                PrimaryEducationDepartmentNecessaryInfoFragment();
        primaryEducationDepartmentNecessaryInfoFragment.setArguments(bundle);
        return primaryEducationDepartmentNecessaryInfoFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = getActivity();
        layoutInflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Bundle bundle = getArguments();
        FRAGMENT_SHOW_TYPE = bundle.getInt("FRAGMENT_SHOW_TYPE");
        FRAGMENT_NAME = bundle.getString("FRAGMENT_NAME");
        newsUpdate = (NewsUpdate) bundle.getSerializable("News Details");

    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragmentView = null;

        switch (FRAGMENT_SHOW_TYPE) {
            case 1:
                switch (FRAGMENT_NAME) {
                    case "Primary Education Department":
                        fragmentView = layoutInflater
                                .inflate(R.layout.primary_education_info_layout,
                                        container, false);
                        break;
                    case "History":
                    case "Future Plan":
                        fragmentView = layoutInflater.inflate
                                (R.layout.primary_education_info_necessary_info_layout,
                                        null, false);
                        break;
                    default:
                        fragmentView = layoutInflater
                                .inflate(R.layout.primary_edu_info_list_view_layout,
                                        container, false);
                        break;
                }


                break;
            case 2:
            case 3:
            case 4:
            case 6:
                fragmentView = layoutInflater
                        .inflate(R.layout.primary_edu_info_list_view_layout, container, false);
                break;
            case 7:
            case 8:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 16:
                fragmentView = layoutInflater
                        .inflate(R.layout.primary_edu_info_list_view_layout, container, false);
                break;
            case 5:
                fragmentView = layoutInflater
                        .inflate(R.layout.vertically_image_with_text_layout, container, false);
                break;
            case 15:
                fragmentView = layoutInflater
                        .inflate(R.layout.category_details_layout, container, false);
                break;
            case 17:
                fragmentView = layoutInflater.inflate
                        (R.layout.primary_education_info_necessary_info_layout,
                                null, false);
                break;
            case 9:
            case 18:
                fragmentView = layoutInflater.inflate(R.layout.text_with_list_view_layout,
                        null, false);
                break;
            default:
                break;


        }
        setFragmentLayoutElements(fragmentView);
        return fragmentView;
    }

    private void setFragmentLayoutElements(View fragmentView) {
        switch (FRAGMENT_SHOW_TYPE) {
            case 1:
                switch (FRAGMENT_NAME) {
                    case "Primary Education Department":
                        setPrimaryEducationInfoView(fragmentView);
                        break;
                    case "History":
                    case "Future Plan":
                        setTextFragmentView(fragmentView);
                    default:
                        listView = (ListView) fragmentView
                                .findViewById(R.id.primary_edu_info_list_view);
                        setListFragmentView(fragmentView);
                        break;
                }
                break;
            case 2:
            case 3:
            case 4:
            case 6:
                listView = (ListView) fragmentView
                        .findViewById(R.id.primary_edu_info_list_view);
                setListFragmentView(fragmentView);
                break;
            case 5:
                setVerticallyImageandTextView(fragmentView);
                break;
            case 7:
            case 8:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 16:
                listView = (ListView) fragmentView
                        .findViewById(R.id.primary_edu_info_list_view);
                setListFragmentView(fragmentView);
                break;
            case 15:
                setNewsDetailsFragmnetView(fragmentView);
                break;
            case 17:
                setTextFragmentView(fragmentView);
                break;
            case 9:
            case 18:
                listView = (ListView) fragmentView
                        .findViewById(R.id.primary_edu_info_list_view);
                setTextFragmentView(fragmentView);
                setListFragmentView(fragmentView);
            default:
                break;

        }
    }

    private void setNewsDetailsFragmnetView(View fragmentView) {
        if (fragmentView != null) {
            TextView txtHeadLine = (TextView) fragmentView.findViewById(R.id.txt_category);
            TextView txtdetails = (TextView) fragmentView.findViewById(R.id.txt_details);
            if (newsUpdate != null) {
                txtHeadLine.setText(newsUpdate.getHeadline());
                String details = "তারিখ:" + newsUpdate.getDate() + "\n\n" +
                        newsUpdate.getDetails();
                txtdetails.setText(details);
            }
        }
    }

    private void setVerticallyImageandTextView(View fragmentView) {
        ImageView imageView = (ImageView) fragmentView.findViewById(R.id.imageView);
        TextView textView = (TextView) fragmentView.findViewById(R.id.txt_primary_info);
        String details = null;
        switch (FRAGMENT_NAME) {
            case "director general":
                DirectorGeneral directorGeneral = DirectorGeneral.getDirectorGeneral();
                Picasso.with(context)
                        .load(directorGeneral.getImageLink())
                        .placeholder(R.drawable.dg)
                        .error(R.drawable.dg)
                        .into(imageView);
                details = directorGeneral.getDetails();
                break;
        }
        textView.setText(details);
    }

    private void setPrimaryEducationInfoView(View fragmentView) {
        PrimaryEducationDepartmentInfo primaryEducationDepartmentInfo =
                PrimaryEducationDepartmentInfo.getPrimaryEducationDepartmentInfo();
        if (primaryEducationDepartmentInfo != null) {
            ImageView imageView = (ImageView) fragmentView.findViewById(R.id.imageView);
            TextView txtAddress = (TextView) fragmentView.findViewById(R.id.txtAddress);
            final TextView txtPhone = (TextView) fragmentView.findViewById(R.id.txtPhone);
            TextView txtFax = (TextView) fragmentView.findViewById(R.id.txtFax);
            final TextView txtEmail = (TextView) fragmentView.findViewById(R.id.txtEmail);
            Picasso.with(context)
                    .load(primaryEducationDepartmentInfo.getImageLink())
                    .error(R.drawable.primary_education_info)
                    .placeholder(R.drawable.primary_education_info)
                    .into(imageView);
            if (checkForValidText(primaryEducationDepartmentInfo.getAddress())) {
                txtAddress.setText(primaryEducationDepartmentInfo.getAddress().trim());
            }
            if (checkForValidText(primaryEducationDepartmentInfo.getPhone())) {
                txtPhone.setText(primaryEducationDepartmentInfo.getPhone().trim());
            }
            if (checkForValidText(primaryEducationDepartmentInfo.getFax())) {
                txtFax.setText(primaryEducationDepartmentInfo.getFax().trim());
            }
            if (checkForValidText(primaryEducationDepartmentInfo.getEmail())) {
                txtEmail.setText(primaryEducationDepartmentInfo.getEmail().trim());
            }
            txtPhone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String text = txtPhone.getText().toString();
                    if (text != null && !text.equals("")) {
                        String msg = "প্রাথমিক শিক্ষা অধিদপ্তরে কল করার জন্য আপনার কল চার্জ প্রযোজ্য হবে ";
                        UserNotifiedDialog userNotifiedDialog = new
                                UserNotifiedDialog(context, "Calling Alert", msg, text);
                        userNotifiedDialog.showDialog();
                    }
                }
            });
            txtEmail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!ConnectivityInfo.isInternetConnectionOn(context)) {
                        Toast.makeText(context, "আপনার ইন্টারনেট সংযোগ বন্ধ",
                                Toast.LENGTH_LONG).show();
                    } else {
                        String email = txtEmail.getText().toString();
                        if (email != null && !email.equals("")) {
                            WebBrowser.sentEmails(context, email);
                        }
                    }
                }
            });

        }

    }

    private void setTextFragmentView(View fragmentView) {
        TextView txtPrimaryEduInfo = (TextView) fragmentView.findViewById(R.id.txt_primary_info);
        String text = null;
        switch (FRAGMENT_NAME) {
            case "History":
                text = History.getHistory().getDetails();
                break;
            case "Future Plan":
                text = FuturePlan.getFuturePlan().getDetails();
                break;
            case "Project 2":
                text = ProjectPedp2.getProjectPedp2().getInfo();
                break;
            case "E Primary System":
                text = "ই-প্রাইমারী স্কুল সিস্টেম " + "\n" +
                        " ই-প্রাইমারী স্কুল সিস্টেমে Log In করতে নির্দিষ্ট বিভাগে  ক্লিক করুন।";
                break;
            case "Project":
                text = "পিইডিপি-৩\n" +
                        "\n" +
                        "তৃতীয় প্রাথমিক শিক্ষা উন্নয়ন কর্মসূচী";
                break;
            default:
                break;

        }
        if (checkForValidText(text)) {
            txtPrimaryEduInfo.setText(text.trim());
            Log.e(getClass().getName(), "Text View Fragment has been added.");
        }
    }

    private boolean checkForValidText(String address) {
        if (address != null && !address.equals("null") && !address.equals("নাই")
                && !address.equals("")) {
            return true;
        }
        return false;
    }

    private void setListFragmentView(View fragmentView) {
        PrimaryEduDeptInfoListViewAdapter primaryEduDeptInfoListViewAdapter = null;
        switch (FRAGMENT_NAME) {
            case "Director Generals Working Period":
                List<WorkingPeriodDirectorGeneral> workingPeriodDirectorGeneralList =
                        WorkingPeriodDirectorGeneral.getWorkingPeriodDtrectorGeneralList();
                primaryEduDeptInfoListViewAdapter = new
                        PrimaryEduDeptInfoListViewAdapter
                        (context, 1, workingPeriodDirectorGeneralList);
                break;
            case "Working Procedure":
                primaryEduDeptInfoListViewAdapter = new PrimaryEduDeptInfoListViewAdapter
                        (context, 2, WorkingProcedure.getWorkingProcedureList());
                break;
            case "Division Office":
                List<DivisionOffice> divisionOfficeList = DivisionOffice.getDivisionOfficeList();
                primaryEduDeptInfoListViewAdapter = new
                        PrimaryEduDeptInfoListViewAdapter(context, 3, divisionOfficeList);
                break;
            case "District Office":
                List<DistrictOffice> districtOfficeList = DistrictOffice.getDistrictOfficeList();
                primaryEduDeptInfoListViewAdapter = new
                        PrimaryEduDeptInfoListViewAdapter(context, 4, districtOfficeList);
                break;
            case "PTI Office":
                List<PtiOffice> ptiOfficeList = PtiOffice.getPtiOfficeList();
                primaryEduDeptInfoListViewAdapter = new
                        PrimaryEduDeptInfoListViewAdapter(context, 5, ptiOfficeList);
                break;
            case "E Primary System":
                List<E_PrimarySystem> e_primarySystemList = E_PrimarySystem
                        .getE_primarySystemList();
                primaryEduDeptInfoListViewAdapter = new
                        PrimaryEduDeptInfoListViewAdapter(context, 6, e_primarySystemList);
                break;
            case "information rights":
                primaryEduDeptInfoListViewAdapter = new
                        PrimaryEduDeptInfoListViewAdapter(context, 7,
                        InformationRights.getInformationRightsList());
                break;
            case "information providing officer":
                primaryEduDeptInfoListViewAdapter = new
                        PrimaryEduDeptInfoListViewAdapter(context, 8,
                        InfoProvidingOfficer.getInfoProvidingOfficerList());
                break;
            case "givense redress system":
                primaryEduDeptInfoListViewAdapter = new
                        PrimaryEduDeptInfoListViewAdapter(context, 9,
                        GivenseRedressSystem.getGivenseRedressSystemList());
                break;
            case "News Update":
                newsUpdateList = NewsUpdate.getNewsUpdateList();
                if (newsUpdateList != null) {
                    primaryEduDeptInfoListViewAdapter = new
                            PrimaryEduDeptInfoListViewAdapter(context, 10, newsUpdateList);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view,
                                                int position, long id) {
                            NewsUpdate newsUpdate = newsUpdateList.get(position);
                            if (newsUpdate != null) {
                                Intent intent = new Intent(context, FragmentActivity.class);
                                intent.putExtra("Fragment Name", "খবর");
                                intent.putExtra("News Update", newsUpdate);
                                context.startActivity(intent);
                            }
                        }
                    });
                }
                break;
            case "rules":
                List<Rules> rulesList = Rules.getRulesList();
                if (rulesList != null) {
                    primaryEduDeptInfoListViewAdapter = new
                            PrimaryEduDeptInfoListViewAdapter
                            (context, 11, rulesList);
                }
                break;
            case "advertisement":
                List<Advertisement> advertisementList = Advertisement.getAdvertisementList();
                if (advertisementList != null) {
                    primaryEduDeptInfoListViewAdapter = new
                            PrimaryEduDeptInfoListViewAdapter
                            (context, 12, advertisementList);
                }
                break;
            case "notice":
                List<Notice> noticeList = Notice.getNoticeList();
                if (noticeList != null) {
                    primaryEduDeptInfoListViewAdapter = new
                            PrimaryEduDeptInfoListViewAdapter
                            (context, 13, noticeList);
                }
                break;
            case "Tender":
                final List<TenderNotice> tenderNoticeList = TenderNotice.getTenderNoticeList();
                if (tenderNoticeList != null) {
                    primaryEduDeptInfoListViewAdapter = new
                            PrimaryEduDeptInfoListViewAdapter
                            (context, 14, tenderNoticeList);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view,
                                                int position, long id) {
                            TenderNotice tenderNotice = tenderNoticeList.get(position);
                            if (tenderNotice != null) {
                                String urlLink = tenderNotice.getUrlLink();
                                if (urlLink != null && !urlLink.equals("null")
                                        && !urlLink.equals("") && !urlLink.equals("নাই")) {
                                    if (!ConnectivityInfo.isInternetConnectionOn(context)) {
                                        Toast.makeText(context, "আপনার ইন্টারনেট সংযোগ বন্ধ",
                                                Toast.LENGTH_LONG).show();
                                    } else {
                                        WebBrowser.openWebBrowser(context, urlLink);
                                    }
                                }
                            }
                        }
                    });
                }
                break;
            case "Project":
                List<ProjectPedp3> projectPedp3List = ProjectPedp3.getProjectPedp3List();
                if (projectPedp3List != null) {
                    primaryEduDeptInfoListViewAdapter = new
                            PrimaryEduDeptInfoListViewAdapter
                            (context, 15, projectPedp3List);
                }
                break;
            case "School Result":
                List<SchoolResultStatictics> schoolResultStaticticsList = SchoolResultStatictics
                        .getSchoolResultStaticticsList();
                if (schoolResultStaticticsList != null) {
                    primaryEduDeptInfoListViewAdapter = new
                            PrimaryEduDeptInfoListViewAdapter
                            (context, 16, schoolResultStaticticsList);

                }
                break;
            case "Madrasha Result":
                List<MadrashaResultStatictics> madrashaResultStaticticsList =
                        MadrashaResultStatictics
                                .getMadrashaResultStaticticsList();
                if (madrashaResultStaticticsList != null) {
                    primaryEduDeptInfoListViewAdapter = new
                            PrimaryEduDeptInfoListViewAdapter
                            (context, 17, madrashaResultStaticticsList);

                }
                break;
            case "Investigation":
                List<MonthlyInvestigation> monthlyInvestigationList = MonthlyInvestigation
                        .getMonthlyInvestigationList();
                if (monthlyInvestigationList != null) {
                    primaryEduDeptInfoListViewAdapter = new
                            PrimaryEduDeptInfoListViewAdapter
                            (context, 18, monthlyInvestigationList);

                }
                break;
            case "Publications":
                List<Publications> publicationsList = Publications.getPublicationsList();
                if (publicationsList != null) {
                    primaryEduDeptInfoListViewAdapter = new
                            PrimaryEduDeptInfoListViewAdapter
                            (context, 19, publicationsList);
                }
                break;
            case "Forms":
                List<Forms> formsList = Forms.getFormsList();
                if (formsList != null) {
                    primaryEduDeptInfoListViewAdapter = new
                            PrimaryEduDeptInfoListViewAdapter
                            (context, 20, formsList);
                }
                break;
            case "Central E Services":
                List<CentralEServices> centralEServicesList = CentralEServices
                        .getCentralEServicesList();
                if (centralEServicesList != null) {
                    primaryEduDeptInfoListViewAdapter = new
                            PrimaryEduDeptInfoListViewAdapter
                            (context, 21, centralEServicesList);
                }
                break;
            case "Internal Services":
                List<InternalServices> internalServicesList = InternalServices
                        .getInternalServicesList();
                if (internalServicesList != null) {
                    primaryEduDeptInfoListViewAdapter = new
                            PrimaryEduDeptInfoListViewAdapter
                            (context, 22, internalServicesList);
                }
                break;
            case "Important Links":
                List<ImportantLink> importantLinkList = ImportantLink.getImportantLinkList();
                if (importantLinkList != null) {
                    primaryEduDeptInfoListViewAdapter = new
                            PrimaryEduDeptInfoListViewAdapter
                            (context, 23, importantLinkList);
                }
                break;
            case "Officers Name List":
                List<OfficersNameList> officersNameLists = OfficersNameList.getOfficersNameLists();
                if (officersNameLists != null && officersNameLists.size() > 0) {
                    if (officersNameLists != null) {
                        primaryEduDeptInfoListViewAdapter = new
                                PrimaryEduDeptInfoListViewAdapter
                                (context, 24, officersNameLists);
                    }
                }
                break;
            default:
                break;
        }
        if (primaryEduDeptInfoListViewAdapter != null) {
            listView.setAdapter(primaryEduDeptInfoListViewAdapter);
        }

    }

}
