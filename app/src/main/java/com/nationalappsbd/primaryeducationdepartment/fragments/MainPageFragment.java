package com.nationalappsbd.primaryeducationdepartment.fragments;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.PhotoGallery;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.PrimaryEducationDepartmentInfo;
import com.nationalappsbd.primaryeducationdepartment.R;
import com.nationalappsbd.primaryeducationdepartment.dialogs.UserNotifiedDialog;
import com.nationalappsbd.primaryeducationdepartment.infos.ConnectivityInfo;
import com.nationalappsbd.primaryeducationdepartment.interfaces.Initializer;
import com.nationalappsbd.primaryeducationdepartment.webServices.WebBrowser;

import java.util.List;

/**
 * Created by shuvojit on 5/24/15.
 */
public class MainPageFragment extends Fragment implements Initializer {


    private Context context;
    private View fragmentView;
    private List<PhotoGallery> photoGalleryList;
    private int[] imageResources;
    private GridView gridView;
    private String[] subMenus;


    public MainPageFragment() {
    }

    public static MainPageFragment getNewInstance() {
        return new MainPageFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        context = getActivity();
        LayoutInflater layoutInflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        fragmentView = layoutInflater.inflate(R.layout.first_page_fragment_layout, null, false);
        initialize();
        //gridView.setOnItemClickListener(this);
        return fragmentView;
    }

    @Override
    public void initialize() {
        if (fragmentView != null) {
            /*Button btn1 = (Button) fragmentView.findViewById(R.id.btn1);
            Button btn2 = (Button) fragmentView.findViewById(R.id.btn2);
            Button btn3 = (Button) fragmentView.findViewById(R.id.btn3);
            Button btn4 = (Button) fragmentView.findViewById(R.id.btn4);
            btn1.setOnClickListener(this);
            btn2.setOnClickListener(this);
            btn3.setOnClickListener(this);
            btn4.setOnClickListener(this);*/
            setPrimaryEducationInfoView(fragmentView);
            photoGalleryList = PhotoGallery.photoGalleryList();
            TypedArray typedArray = context.getResources().obtainTypedArray(R.array.photo_gallery);
            if (typedArray != null && typedArray.length() > 0) {
                imageResources = new int[typedArray.length()];
                for (int i = 0; i < typedArray.length(); i++) {
                    imageResources[i] = typedArray.getResourceId(i, 0);
                }
                typedArray.recycle();
            }
            setImageSlider();
           /* gridView = (GridView) fragmentView.findViewById(R.id.gridView);*/
//            subMenus = context.getResources().getStringArray(R.array.main_page_submenus);
           /* PrimaryEduInfoGridViewAdapter primaryEduInfoGridViewAdapter = new
                    PrimaryEduInfoGridViewAdapter(context, subMenus);
            if (primaryEduInfoGridViewAdapter != null) {
                gridView.setAdapter(primaryEduInfoGridViewAdapter);
            }*/
        }
    }

    private void setImageSlider() {
        SliderLayout imageSliderLayout = (SliderLayout) fragmentView
                .findViewById(R.id.image_slider);
        for (int i = 0; i < imageResources.length; i++) {
            TextSliderView textSliderView = new TextSliderView(context);
            textSliderView
                    .description(photoGalleryList.get(i).getDescription())
                    .image(imageResources[i])
                    .setScaleType(BaseSliderView.ScaleType.Fit);
            imageSliderLayout.addSlider(textSliderView);
        }
        if (ConnectivityInfo.isInternetConnectionOn(context)) {
            for (int i = imageResources.length; i < photoGalleryList.size(); i++) {
                PhotoGallery photoGallery = photoGalleryList.get(i);
                if (!photoGallery.getImageLink().equals("") ||
                        photoGallery.getImageLink() != null) {
                    TextSliderView textSliderView = new TextSliderView(context);
                    textSliderView
                            .description(photoGallery.getDescription())
                            .image(photoGallery.getImageLink())
                            .setScaleType(BaseSliderView.ScaleType.Fit);
                    imageSliderLayout.addSlider(textSliderView);
                }
            }
        }
        imageSliderLayout.setPresetTransformer(SliderLayout.Transformer.FlipHorizontal);
        imageSliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        imageSliderLayout.setCustomAnimation(new DescriptionAnimation());
        imageSliderLayout.setDuration(5000);
    }

    /* @Override
     public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
         Intent intent = new Intent(context, FragmentActivity.class);
         intent.putExtra("Fragment Name", subMenus[position]);
         context.startActivity(intent);

     }
 */
    private void setPrimaryEducationInfoView(View fragmentView) {
        PrimaryEducationDepartmentInfo primaryEducationDepartmentInfo =
                PrimaryEducationDepartmentInfo.getPrimaryEducationDepartmentInfo();
        if (primaryEducationDepartmentInfo != null) {
           /* ImageView imageView = (ImageView) fragmentView.findViewById(R.id.imageView);*/
            TextView txtAddress = (TextView) fragmentView.findViewById(R.id.txtAddress);
            final TextView txtPhone = (TextView) fragmentView.findViewById(R.id.txtPhone);
            TextView txtFax = (TextView) fragmentView.findViewById(R.id.txtFax);
            final TextView txtEmail = (TextView) fragmentView.findViewById(R.id.txtEmail);
           /* Picasso.with(context)
                    .load(primaryEducationDepartmentInfo.getImageLink())
                    .error(R.drawable.primary_education_info)
                    .placeholder(R.drawable.primary_education_info)
                    .into(imageView);*/
            if (checkForValidText(primaryEducationDepartmentInfo.getAddress())) {
                txtAddress.setText(primaryEducationDepartmentInfo.getAddress().trim());
            }
            if (checkForValidText(primaryEducationDepartmentInfo.getPhone())) {
                txtPhone.setText(primaryEducationDepartmentInfo.getPhone().trim());
            }
            if (checkForValidText(primaryEducationDepartmentInfo.getFax())) {
                txtFax.setText(primaryEducationDepartmentInfo.getFax().trim());
            }
            if (checkForValidText(primaryEducationDepartmentInfo.getEmail())) {
                txtEmail.setText(primaryEducationDepartmentInfo.getEmail().trim());
            }
            txtPhone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String text = txtPhone.getText().toString();
                    if (text != null && !text.equals("")) {
                        String msg = "প্রাথমিক শিক্ষা অধিদপ্তরে কল করার জন্য আপনার কল চার্জ প্রযোজ্য হবে ";
                        UserNotifiedDialog userNotifiedDialog = new
                                UserNotifiedDialog(context, "Calling Alert", msg, text);
                        userNotifiedDialog.showDialog();
                    }
                }
            });
            txtEmail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!ConnectivityInfo.isInternetConnectionOn(context)) {
                        Toast.makeText(context, "আপনার ইন্টারনেট সংযোগ বন্ধ",
                                Toast.LENGTH_LONG).show();
                    } else {
                        String email = txtEmail.getText().toString();
                        if (email != null && !email.equals("")) {
                            WebBrowser.sentEmails(context, email);
                        }
                    }
                }
            });

        }

    }


    private boolean checkForValidText(String address) {
        if (address != null && !address.equals("null") && !address.equals("নাই")
                && !address.equals("")) {
            return true;
        }
        return false;
    }

   /* @Override
    public void onClick(View v) {
        int ID = v.getId();
        Intent intent = null;
        switch (ID) {
            case R.id.btn1:
                intent = new Intent(context, FragmentActivity.class);
                intent.putExtra("Fragment Name", subMenus[0]);
                context.startActivity(intent);
                break;
            case R.id.btn2:
                intent = new Intent(context, FragmentActivity.class);
                intent.putExtra("Fragment Name", subMenus[1]);
                context.startActivity(intent);
                break;
            case R.id.btn3:
                intent = new Intent(context, FragmentActivity.class);
                intent.putExtra("Fragment Name", subMenus[2]);
                context.startActivity(intent);
                break;
            case R.id.btn4:
                intent = new Intent(context, FragmentActivity.class);
                intent.putExtra("Fragment Name", subMenus[3]);
                context.startActivity(intent);
                break;
        }
    }*/
}
