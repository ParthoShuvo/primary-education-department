package com.nationalappsbd.primaryeducationdepartment.webServices;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

/**
 * Created by shuvojit on 5/26/15.
 */
public class WebBrowser {

    public static void openWebBrowser(Context context, String urlLink) {
        if (context != null && urlLink != null && !urlLink.startsWith("<p>") &&
                !urlLink.equals("null")) {
            if (!urlLink.startsWith("http://")) {
                urlLink = "http://" + urlLink;
            }
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlLink));
            context.startActivity(intent);
        }
    }

    public static void sentEmails(Context context, String emailClient) {
        if (context != null && emailClient != null && emailClient.endsWith(".com")) {
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
            emailIntent.setData(Uri.parse("mailto:" + emailClient));
            context.startActivity(emailIntent);
        }
    }
}
