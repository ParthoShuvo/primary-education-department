package com.nationalappsbd.primaryeducationdepartment.activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.PrimaryEducationDepartmentInfo;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.Rules;
import com.nationalappsbd.primaryeducationdepartment.R;
import com.nationalappsbd.primaryeducationdepartment.adapters.listViewAdapters.NavDrawerListViewAdapter;
import com.nationalappsbd.primaryeducationdepartment.dialogs.UserNotifiedDialog;
import com.nationalappsbd.primaryeducationdepartment.fragments.PrimaryEducationDepartmentInGoogleMapFragment;
import com.nationalappsbd.primaryeducationdepartment.fragments.PrimaryEducationDepartmentNecessaryInfoFragment;
import com.nationalappsbd.primaryeducationdepartment.fragments.SlidingTabViewFragment;
import com.nationalappsbd.primaryeducationdepartment.infos.ConnectivityInfo;
import com.nationalappsbd.primaryeducationdepartment.interfaces.Initializer;


public class MainActivity extends ActionBarActivity implements Initializer,
        AdapterView.OnItemClickListener {

    private Toolbar toolbar;
    private FragmentManager fragmentManager;
    private DrawerLayout navDrawerLayout;
    private ListView navDrawerListView;
    private ActionBarDrawerToggle navDrawerListener;
    private NavDrawerListViewAdapter navDrawerListViewItemAdapter;
    private String[] navDrawerMenuItemNames;
    private int menuPosition = -1;
    private TextView toolbarTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            setContentView(R.layout.activity_main);
            initialize();
            setNavDrawerListViewAdapter();
            navDrawerListView.setItemChecked(0, true);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (navDrawerListener != null) {
            navDrawerListener.onOptionsItemSelected(item);
        }
        int id = item.getItemId();
        Intent intent = null;
        UserNotifiedDialog userNotifiedDialog = null;
        String msg = null;
        switch (id) {
            case R.id.about_menu:
                msg = "এই অ্যাপ্লিকেশনটি প্রাথমিক শিক্ষা অধিদপ্তরের জন্য";
                userNotifiedDialog =
                        new UserNotifiedDialog(MainActivity.this, "Description Alert", msg);
                userNotifiedDialog.showDialog();
                break;
            case R.id.updateDataMenu:
                if (!ConnectivityInfo.isInternetConnectionOn(MainActivity.this)) {
                    Toast.makeText(MainActivity.this,
                            "আপনার ইন্টারনেট সংযোগ বন্ধ", Toast.LENGTH_LONG).show();
                } else {
                    msg = "আপনি কি তথ্য আপডেট করতে চান ?";
                    userNotifiedDialog = new UserNotifiedDialog(MainActivity.this,
                            "Service Starting Alert", msg);
                    userNotifiedDialog.showDialog();
                }
                break;
            case android.R.id.home:
                break;
           /* case R.id.googleMapMenu:
                if (!ConnectivityInfo.isInternetConnectionOn(MainActivity.this)) {
                    Toast.makeText(MainActivity.this,
                            "আপনার ইন্টারনেট সংযোগ বন্ধ", Toast.LENGTH_LONG).show();
                } else {
                    intent = new Intent(MainActivity.this, FragmentActivity.class);
                    intent.putExtra("Fragment Name", "মানচিত্রে অবস্থান");
                    startActivity(intent);
                }
                break;
            case R.id.facebookMenu:
                if (!ConnectivityInfo.isInternetConnectionOn(MainActivity.this)) {
                    Toast.makeText(MainActivity.this,
                            "আপনার ইন্টারনেট সংযোগ বন্ধ", Toast.LENGTH_LONG).show();
                } else {
                    intent = new Intent(MainActivity.this, FragmentActivity.class);
                    intent.putExtra("Fragment Name", "ফেইসবুক পেইজ");
                    startActivity(intent);
                }
                break;
            case R.id.webPageMenu:
                if (!ConnectivityInfo.isInternetConnectionOn(MainActivity.this)) {
                    Toast.makeText(MainActivity.this,
                            "আপনার ইন্টারনেট সংযোগ বন্ধ", Toast.LENGTH_LONG).show();
                } else {
                    intent = new Intent(MainActivity.this, FragmentActivity.class);
                    intent.putExtra("Fragment Name", "ওয়েব পেইজ");
                    startActivity(intent);
                }
                break;
            case R.id.webMailMenu:
                if (!ConnectivityInfo.isInternetConnectionOn(MainActivity.this)) {
                    Toast.makeText(MainActivity.this,
                            "আপনার ইন্টারনেট সংযোগ বন্ধ", Toast.LENGTH_LONG).show();
                } else {
                    intent = new Intent(MainActivity.this, FragmentActivity.class);
                    intent.putExtra("Fragment Name", "ওয়েব মেইল");
                    startActivity(intent);
                }
                break;
            case R.id.infoPaperDownload:
                if (!ConnectivityInfo.isInternetConnectionOn(MainActivity.this)) {
                    Toast.makeText(MainActivity.this,
                            "আপনার ইন্টারনেট সংযোগ বন্ধ", Toast.LENGTH_LONG).show();
                } else {
                    String url = InfoCommitteeDecision.getInfoCommitteeDecision().getUrlLink();
                    if (url != null && !url.equals("") && !url.equals("null")) {
                        intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        startActivity(intent);
                    }
                }
                break;*/
        }
        return true;


    }

    private void setNavDrawerListViewAdapter() {
        if (navDrawerListViewItemAdapter != null) {
            navDrawerListView.setAdapter(navDrawerListViewItemAdapter);
            navDrawerListView.setOnItemClickListener(MainActivity.this);
        }
    }

    @Override
    public void initialize() {
        toolbar = (Toolbar) findViewById(R.id.actionbarToolbar);
        toolbarTextView = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar.setTitle("");
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/BenSenHandwriting.ttf");
        toolbarTextView.setTypeface(typeface);
        toolbarTextView.setText(R.string.app_name);
        fragmentManager = getSupportFragmentManager();
        setSupportActionBar(toolbar);
        navDrawerLayout = (DrawerLayout) findViewById(R.id.nav_drawer_layout);
        navDrawerListView = (ListView) findViewById(R.id.nav_drawer_list_menu);
        setNavigationDrawerListener();
        navDrawerListViewItemAdapter = new NavDrawerListViewAdapter(MainActivity.this);
        navDrawerMenuItemNames = getResources().getStringArray(R.array.navMenuItem);
        setFragment(0);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void setNavigationDrawerListener() {
        int drawerOpenMsg = R.string.nav_drawer_open_msg;
        int drawerCloseMsg = R.string.nav_drawer_close_msg;
        navDrawerListener = new ActionBarDrawerToggle(MainActivity.this,
                navDrawerLayout, drawerOpenMsg, drawerCloseMsg) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                Log.e(getClass().getName(), "Navigation drawer is opened");
            }
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                Log.e(getClass().getName(), "Navigation drawer closed");
            }
        };

        if (navDrawerListener != null && navDrawerLayout != null) {
            Log.e(getClass().getName(), "Navigation drawer listener has been added");
            navDrawerListener.setHomeAsUpIndicator(R.drawable.ic_drawer);
            navDrawerLayout.setDrawerListener(navDrawerListener);
        }
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (navDrawerListener != null) {
            navDrawerListener.onConfigurationChanged(newConfig);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (navDrawerListener != null) {
            navDrawerListener.syncState();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.e(getClass().getName(), navDrawerMenuItemNames[position] + "Clicked");
        setFragment(position);
        navDrawerLayout.closeDrawers();
    }


    private void setFragment(int position) {
        String menuName = navDrawerMenuItemNames[position];
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment fragment = null;
        boolean fragmentChangeFlag = false;
        switch (menuName) {
            /*case "মূলপাতা":
                if (position != menuPosition) {
                    fragment = SlidingTabViewFragment
                            .getNewInstance(menuName);
                    menuPosition = position;
                    fragmentChangeFlag = true;
                }
                break;*/
            case "মূলপাতা":
                if (position != menuPosition) {
                    fragment = SlidingTabViewFragment
                            .getNewInstance(menuName);
                    menuPosition = position;
                    fragmentChangeFlag = true;
                }
                break;
            case "সংবাদ ও অন্যান্য":
                if (position != menuPosition) {
                    fragment = SlidingTabViewFragment.getNewInstance(menuName);
                    fragmentChangeFlag = true;
                    menuPosition = position;
                }
                break;
            case "আইন ও বিধিমালা":
                Log.e(getClass().getName(), Rules.getRulesList().size() + " ");
                if (position != menuPosition) {
                    fragment = SlidingTabViewFragment
                            .getNewInstance(menuName);
                    menuPosition = position;
                    fragmentChangeFlag = true;
                }
                break;
            case "প্রকল্প":
                if (position != menuPosition) {
                    fragment = SlidingTabViewFragment.getNewInstance(menuName);
                    fragmentChangeFlag = true;
                    menuPosition = position;
                }
                break;
            case "প্রকাশনা ও অন্যান্য":
                if (position != menuPosition) {
                    fragment = SlidingTabViewFragment.getNewInstance(menuName);
                    fragmentChangeFlag = true;
                    menuPosition = position;
                }
                break;
            case "নোটিশ বোর্ড":
                if (position != menuPosition) {
                    fragment = PrimaryEducationDepartmentNecessaryInfoFragment
                            .getNewInstance(7, "notice");
                    fragmentChangeFlag = true;
                    menuPosition = position;
                }
                break;
            case "খবর":
                if (position != menuPosition) {
                    fragment = PrimaryEducationDepartmentNecessaryInfoFragment
                            .getNewInstance(4, "News Update");
                    fragmentChangeFlag = true;
                    menuPosition = position;
                }
                break;
            case "অধীনস্ত অফিসসমূহ":
                if (position != menuPosition) {
                    fragment = SlidingTabViewFragment.getNewInstance(menuName);
                    fragmentChangeFlag = true;
                    menuPosition = position;
                }
                break;
            case "তথ্য অধিকার":
                if (position != menuPosition) {
                    fragment = SlidingTabViewFragment.getNewInstance(menuName);
                    fragmentChangeFlag = true;
                    menuPosition = position;
                }
                break;
            case "কর্মকর্তাবৃন্দের তালিকা":
                if (position != menuPosition) {
                    fragment = PrimaryEducationDepartmentNecessaryInfoFragment
                            .getNewInstance(8, "Officers Name List");
                    fragmentChangeFlag = true;
                    menuPosition = position;
                }
                break;
            case "মানচিত্রে অবস্থান":
                if (position != menuPosition) {
                    PrimaryEducationDepartmentInfo primaryEducationDepartmentInfo =
                            PrimaryEducationDepartmentInfo.getPrimaryEducationDepartmentInfo();
                    fragment = PrimaryEducationDepartmentInGoogleMapFragment
                            .newInstance(primaryEducationDepartmentInfo,
                                    primaryEducationDepartmentInfo.getLatitude(),
                                    primaryEducationDepartmentInfo.getLongitude(),
                                    10.5f);
                    fragmentChangeFlag = true;
                    menuPosition = position;
                }
                break;
            default:
                break;

        }
        if (fragmentChangeFlag) {
            fragmentTransaction.replace(R.id.fragment_container, fragment).commit();
            toolbarTextView.setText(menuName);
        }
    }


    @Override
    public void onBackPressed() {
        String msg = "আপনি এই অ্যাপ থেকে প্রস্থান করবেন ?";
        UserNotifiedDialog userNotifiedDialog =
                new UserNotifiedDialog(MainActivity.this, "Exiting Alert", msg);
        userNotifiedDialog.showDialog();
    }

}
