package com.nationalappsbd.primaryeducationdepartment.interfaces;

/**
 * Created by shuvojit on 5/14/15.
 */
public interface SubMenuNamesInterface {

    public final static String INTERNAL_E_SERVICES = "অভ্যন্তরীণ ই-সেবা";

    public final static String TENDER = "টেন্ডার ও কোটেশান";

    public final static String CENTRAL_E_SERVICES = "কেন্দ্রীয় ই-সেবা";

    public final static String IMPORTANT_LINK = "গুরুত্বপূর্ণ লিংক ";

    public final static String DIRECTOR_GENERAL = "মহাপরিচালক";

    public final static String PEDP_3 = "পিইডিপি-৩";

    public final static String PRIMARY_SCHOOL_RESULT = "প্রাথমিক শিক্ষা সমাপনী পরীক্ষা- ২০১৪ এর পরিসংখ্যান";

    public final static String PRIMARY_MADRASHA_RESULT = "ইবতেদায়ী শিক্ষা সমাপনী পরীক্ষা- ২০১৪ এর পরিসংখ্যান";

    public final static String MONTHLY_INVESTIGATIONS = "মাসিক প্রতিবেদন ";

    public final static String PUBLICATIONS = "প্রকাশনাসমূহ ";

    public final static String FORMS = "ফরম";

    public final static String DIVISION_OFFICE = "বিভাগীয় অফিস";

    public final static String DISTRICT_OFFICE = "জেলা অফিস";

    public final static String PTI_OFFICE = "পিটিআই অফিস";

    public final static String E_PRIMARY_SYSTEM = "প্রাথমিক বিদ্যালয়";

    public final static String RULES_AND_REGULATIONS = "নীতিমালা ";

    public final static String ADVERTISEMENTS = "প্রজ্ঞাপন";

    public final static String PRIMARY_EDUCATION_DIRECTORIATE = "প্রাথমিক শিক্ষা অধিদপ্তর";

    public final static String HISTORY = "ইতিহাস";

    public final static String FUTURE_PLAN = "ভবিষৎ পরিকল্পনা";

    public final static String DIRECTOR_GENERALS_WORKING_PERIOD = "মহাপরিচালকের কার্যকাল";

    public final static String WORKING_PROCEDURE_1 = "কার্যক্রম 1";

    public final static String WORKING_PROCEDURE_2 = "কার্যক্রম 2";

    public final static String WORKING_PROCEDURE_3 = "কার্যক্রম 3";

    public final static String WORKING_PROCEDURE_4 = "কার্যক্রম 4";

    public final static String WORKING_PROCEDURE_5 = "কার্যক্রম 5";

    public final static String WORKING_PROCEDURE_6 = "কার্যক্রম 6";

    public final static String WORKING_PROCEDURE_7 = "কার্যক্রম 7";

    public final static String WORKING_PROCEDURE_8 = "কার্যক্রম 8";

    public final static String WORKING_PROCEDURE_9 = "কার্যক্রম 9";

    public final static String WORKING_PROCEDURE_10 = "কার্যক্রম 10";

    public final static String WORKING_PROCEDURE_11 = "কার্যক্রম 11";

    public final static String INFO_RIGHTS = "তথ্য অধিকার";

    public final static String INFO_PROVIDING_OFFICER = "তথ্য প্রদানকারী কর্মকর্তা";

    public final static String INFO_COMMITTEE_DECISION = "তথ্য অধিকার কমিটির সিদ্ধান্ত";

    public final static String GIVENSE_REDRESS_SYSTEM = "গ্রিভেন্স রিড্রেস সিস্টেম";

    public final static String TENDER_NEWS = "দরপত্র বিজ্ঞপ্তি ";

}
