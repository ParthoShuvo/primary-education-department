package com.nationalappsbd.primaryeducationdepartment.backgroundServices;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.Advertisement;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.CentralEServices;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.DirectorGeneral;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.DistrictOffice;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.DivisionOffice;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.E_PrimarySystem;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.Forms;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.FuturePlan;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.GivenseRedressSystem;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.History;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.ImportantLink;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.InfoCommitteeDecision;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.InfoProvidingOfficer;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.InformationRights;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.InternalServices;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.MadrashaResultStatictics;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.MonthlyInvestigation;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.NewsUpdate;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.Notice;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.PhotoGallery;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.PrimaryEducationDepartmentInfo;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.ProjectPedp3;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.PtiOffice;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.Publications;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.Rules;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.SchoolResultStatictics;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.Statictics;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.TenderNotice;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.WorkingPeriodDirectorGeneral;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.WorkingProcedure;
import com.nationalappsbd.primaryeducationdepartment.R;
import com.nationalappsbd.primaryeducationdepartment.dialogs.UserNotifiedDialog;
import com.nationalappsbd.primaryeducationdepartment.gsons.national500APIGson.GalleryImageContents;
import com.nationalappsbd.primaryeducationdepartment.gsons.national500APIGson.GalleryImages;
import com.nationalappsbd.primaryeducationdepartment.gsons.national500APIGson.Menu;
import com.nationalappsbd.primaryeducationdepartment.gsons.national500APIGson.MenuContents;
import com.nationalappsbd.primaryeducationdepartment.gsons.national500APIGson.SubMenu;
import com.nationalappsbd.primaryeducationdepartment.gsons.national500APIGson.SubMenuContents;
import com.nationalappsbd.primaryeducationdepartment.interfaces.JsonFields;
import com.nationalappsbd.primaryeducationdepartment.interfaces.JsonLinks;
import com.nationalappsbd.primaryeducationdepartment.interfaces.MenuIDsInterface;
import com.nationalappsbd.primaryeducationdepartment.interfaces.SubMenuNamesInterface;
import com.nationalappsbd.primaryeducationdepartment.webServices.JsonPostClient;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by shuvojit on 5/11/15.
 */
public class BackGroundAsynTask extends AsyncTask<Void, Void, Boolean> implements JsonLinks,
        JsonFields, MenuIDsInterface, SubMenuNamesInterface {


    private final String APP_ID = "260";
    private boolean updated = false;
    private Context context;

    public BackGroundAsynTask() {


    }

    @Override
    protected Boolean doInBackground(Void... params) {

        try {

            getMenuUpdate();
            getSubMenuUpdate();
            getImageUpdate();
            context = UserNotifiedDialog.context;
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (updated) {
            return true;
        }
        return false;
    }

    private void getSubMenuUpdate() {
        String menuID[] = new String[]{INTERNAL_OFFICES, RULES, HOME_PAGE_ID, PROJECT_ID,
                PUBLICATIONS_AND_NEWS, ABOUT_US, INFORIGHTS, NEWS_AND_OTHERS};
        Log.e(getClass().getName(), menuID.length + "");
        try {
            for (int i = 0; i < menuID.length; i++) {
                Log.e(getClass().getName(), menuID[i]);
                JSONObject jsonput = new JSONObject();
                jsonput.put(JsonFields.app_id, APP_ID);
                jsonput.put(JsonFields.menuId, menuID[i]);
                JSONObject jsonsend = JsonPostClient.SendHttpPost(JsonLinks.submenuList,
                        jsonput);
                if (jsonsend != null) {
                    String jsonString = jsonsend.toString();
                    Log.e(getClass().getName(), jsonString);
                    Gson gson = new Gson();
                    SubMenu subMenu = gson.fromJson(jsonString, SubMenu.class);
                    if (subMenu != null) {
                        ArrayList<SubMenuContents> subMenuContentsArrayList = subMenu
                                .getSubMenuContentsArrayList();
                        getAllSubMenusElements(subMenuContentsArrayList);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getAllSubMenusElements(ArrayList<SubMenuContents>
                                                subMenuContentsArrayList) {
        if (subMenuContentsArrayList != null
                && subMenuContentsArrayList.size() > 0) {
            for (int i = 0; i < subMenuContentsArrayList.size(); i++) {
                SubMenuContents subMenuContents = subMenuContentsArrayList.get(i);
                if (subMenuContents != null) {
                    String subMenuName = subMenuContents.getSubMenuName();
                    String contents = subMenuContents.getContent();
                    if (subMenuName != null && contents != null) {
                        Log.e(getClass().getName(), subMenuName);
                        Log.e(getClass().getName(), contents);
                        if (subMenuContents.getSubMenuID().equals("854") ||
                                subMenuContents.getSubMenuID().equals("855")) {
                            Statictics statictics = null;
                            String data = null;
                            List<String> dataList = null;
                            int size;
                            if (subMenuContents.getSubMenuID().equals("854")) {
                                dataList = decodeStringList(contents);
                                size = SchoolResultStatictics
                                        .getSchoolResultStaticticsList()
                                        .size();
                                if (dataList != null && dataList.size() >= (size * 2)) {
                                    if (dataList.size() % 2 != 0) {
                                        dataList.remove(dataList.size() - 1);
                                    }
                                    updateSchoolResultStatictics(dataList);
                                    updated = true;
                                }
                                statictics = Statictics.load(Statictics.class, 1);

                            } else {
                                dataList = decodeStringList(contents);
                                size = MadrashaResultStatictics
                                        .getMadrashaResultStaticticsList()
                                        .size();
                                if (dataList != null && dataList.size() >= (size * 2)) {
                                    if (dataList.size() % 2 != 0) {
                                        dataList.remove(dataList.size() - 1);
                                    }
                                    updateMadrashaResultStatictics(dataList);
                                    updated = true;
                                }
                                statictics = Statictics.load(Statictics.class, 2);
                            }
                            statictics.setSubMenu(subMenuName);
                            statictics.save();
                        } else {

                            findSubMenuContents(subMenuName, contents);
                        }

                    } else {
                        Log.e(getClass().getName(), "Contents are null");
                    }
                }
            }
        }
    }

    private void findSubMenuContents(String subMenuName, String contents) {
        String data = null;
        List<String> dataList = null;
        int size = 0;
        switch (subMenuName) {
            case PEDP_3:
                dataList = decodeStringList(contents);
                size = ProjectPedp3.getProjectPedp3List().size();
                if (dataList != null && dataList.size() >= (size * 2)) {
                    if (dataList.size() % 2 != 0) {
                        dataList.remove(dataList.size() - 1);
                    }
                    updateProjectPEDP3(dataList);
                    updated = true;
                }
                break;
            /*case PRIMARY_SCHOOL_RESULT:
                dataList = decodeStringList(contents);
                size = SchoolResultStatictics.getSchoolResultStaticticsList().size();
                if (dataList != null && dataList.size() >= (size * 2)) {
                    if (dataList.size() % 2 != 0) {
                        dataList.remove(dataList.size() - 1);
                    }
                    updateSchoolResultStatictics(dataList);
                    updated = true;
                }
                break;
            case PRIMARY_MADRASHA_RESULT:
                dataList = decodeStringList(contents);
                size = MadrashaResultStatictics.getMadrashaResultStaticticsList().size();
                if (dataList != null && dataList.size() >= (size * 2)) {
                    if (dataList.size() % 2 != 0) {
                        dataList.remove(dataList.size() - 1);
                    }
                    updateMadrashaResultStatictics(dataList);
                    updated = true;
                }
                break;*/
            case MONTHLY_INVESTIGATIONS:
                dataList = decodeStringList(contents);
                size = MonthlyInvestigation.getMonthlyInvestigationList().size();
                if (dataList != null && dataList.size() >= (size * 2)) {
                    if (dataList.size() % 2 != 0) {
                        dataList.remove(dataList.size() - 1);
                    }
                    updateMOnthlyInvestigation(dataList);
                    updated = true;
                }
                break;
            case PUBLICATIONS:
                dataList = decodeStringList(contents);
                size = Publications.getPublicationsList().size();
                if (dataList != null && dataList.size() >= (size * 2)) {
                    if (dataList.size() % 2 != 0) {
                        dataList.remove(dataList.size() - 1);
                    }
                    updatePublicationList(dataList);
                    updated = true;
                }
                break;
            case FORMS:
                dataList = decodeStringList(contents);
                size = Forms.getFormsList().size();
                if (dataList != null && dataList.size() >= (size * 2)) {
                    if (dataList.size() % 2 != 0) {
                        dataList.remove(dataList.size() - 1);
                    }
                    updateFormsList(dataList);
                    updated = true;
                }
                break;
            case DIVISION_OFFICE:
                dataList = decodeStringList(contents);
                size = DivisionOffice.getDivisionOfficeList().size();
                if (dataList != null && dataList.size() >= (size * 6)) {
                    if (dataList.size() % 6 != 0) {
                        dataList.remove(dataList.size() - 1);
                    }
                    updateDivisionList(dataList);
                    updated = true;
                }
                break;
            case DISTRICT_OFFICE:
                dataList = decodeStringList(contents);
                size = DistrictOffice.getDistrictOfficeList().size();
                if (dataList != null && dataList.size() >= (size * 6)) {
                    if (dataList.size() % 6 != 0) {
                        dataList.remove(dataList.size() - 1);
                    }
                    updateDistrictOfficeList(dataList);
                    updated = true;
                }
                break;
            case PTI_OFFICE:
                dataList = decodeStringList(contents);
                size = PtiOffice.getPtiOfficeList().size();
                if (dataList != null && dataList.size() >= (size * 6)) {
                    if (dataList.size() % 6 != 0) {
                        dataList.remove(dataList.size() - 1);
                    }
                    updatePtiOfficeList(dataList);
                    updated = true;
                }
                break;
            case E_PRIMARY_SYSTEM:
                dataList = decodeStringList(contents);
                size = E_PrimarySystem.getE_primarySystemList().size();
                if (dataList != null && dataList.size() >= (size * 2)) {
                    if (dataList.size() % 2 != 0) {
                        dataList.remove(dataList.size() - 1);
                    }
                    updateEPrimarySystemList(dataList);
                    updated = true;
                }
                break;
            case RULES_AND_REGULATIONS:
                dataList = decodeStringList(contents);
                size = Rules.getRulesList().size();
                if (dataList != null && dataList.size() >= (size * 2)) {
                    if (dataList.size() % 2 != 0) {
                        dataList.remove(dataList.size() - 1);
                    }
                    updateRulesList(dataList);
                    updated = true;
                }
                break;
            case ADVERTISEMENTS:
                dataList = decodeStringList(contents);
                size = Advertisement.getAdvertisementList().size();
                if (dataList != null && dataList.size() >= (size * 2)) {
                    if (dataList.size() % 2 != 0) {
                        dataList.remove(dataList.size() - 1);
                    }
                    updateAdvertisementList(dataList);
                    updated = true;
                }
                break;
            case INTERNAL_E_SERVICES:
                dataList = decodeStringList(contents);
                size = InternalServices.getInternalServicesList().size();
                if (dataList != null && dataList.size() >= (size * 2)) {
                    if (dataList.size() % 2 != 0) {
                        dataList.remove(dataList.size() - 1);
                    }
                    updateInternalServicesList(dataList);
                    updated = true;
                }
                break;
            case CENTRAL_E_SERVICES:
                dataList = decodeStringList(contents);
                size = CentralEServices.getCentralEServicesList().size();
                if (dataList != null && dataList.size() >= (size * 2)) {
                    if (dataList.size() % 2 != 0) {
                        dataList.remove(dataList.size() - 1);
                    }
                    updateCentralEServicesList(dataList);
                    updated = true;
                }
                break;
            case IMPORTANT_LINK:
                dataList = decodeStringList(contents);
                size = ImportantLink.getImportantLinkList().size();
                if (dataList != null && dataList.size() >= (size * 2)) {
                    if (dataList.size() % 2 != 0) {
                        dataList.remove(dataList.size() - 1);
                    }
                    updateImportantLinkList(dataList);
                    updated = true;
                }
                break;
            case DIRECTOR_GENERAL:
                IMPORTANT_LINK:
                dataList = decodeStringList(contents);
                if (dataList != null && dataList.size() == 2) {
                    updateDirectorGeneral(dataList);
                    updated = true;
                }
                break;
            case TENDER:
                dataList = decodeStringList(contents);
                size = TenderNotice.getTenderNoticeList().size();
                if (dataList != null && dataList.size() >= (size * 3)) {
                    if (dataList.size() % 3 != 0) {
                        dataList.remove(dataList.size() - 1);
                    }
                    updateTenderNoticeList(dataList);
                    updated = true;
                }
                break;
            case PRIMARY_EDUCATION_DIRECTORIATE:
                dataList = decodeStringList(contents);
                if (dataList != null && dataList.size() == 7) {
                    updatePrimaryEducationDirectorirate(dataList);
                    updated = true;
                }
                break;
            case HISTORY:
                data = decodeString(contents);
                if (data != null && !data.equals("")) {
                    updateHistory(data);
                }
                break;
            case FUTURE_PLAN:
                data = decodeString(contents);
                if (data != null && !data.equals("")) {
                    updateFuturePlan(data);
                }
                break;
            case DIRECTOR_GENERALS_WORKING_PERIOD:
                dataList = decodeStringList(contents);
                size = WorkingPeriodDirectorGeneral
                        .getWorkingPeriodDtrectorGeneralList().size();
                if (dataList != null && dataList.size() >= (size * 3)) {
                    if (dataList.size() % 3 != 0) {
                        dataList.remove(dataList.size() - 1);
                    }
                    updateWorkingPeriodOfDirectiorGeneralsList(dataList);
                    updated = true;
                }
                break;
            case WORKING_PROCEDURE_1:
                data = decodeString(contents);
                if (data != null && !data.equals("")) {
                    updateWorkingProcedure(data, 1);
                }
                break;
            case WORKING_PROCEDURE_2:
                data = decodeString(contents);
                if (data != null && !data.equals("")) {
                    updateWorkingProcedure(data, 2);
                }
                break;
            case WORKING_PROCEDURE_3:
                data = decodeString(contents);
                if (data != null && !data.equals("")) {
                    updateWorkingProcedure(data, 3);
                }
                break;
            case WORKING_PROCEDURE_4:
                data = decodeString(contents);
                if (data != null && !data.equals("")) {
                    updateWorkingProcedure(data, 4);
                }
                break;
            case WORKING_PROCEDURE_5:
                data = decodeString(contents);
                if (data != null && !data.equals("")) {
                    updateWorkingProcedure(data, 5);
                }
                break;
            case WORKING_PROCEDURE_6:
                data = decodeString(contents);
                if (data != null && !data.equals("")) {
                    updateWorkingProcedure(data, 6);
                }
                break;
            case WORKING_PROCEDURE_7:
                data = decodeString(contents);
                if (data != null && !data.equals("")) {
                    updateWorkingProcedure(data, 7);
                }
                break;
            case WORKING_PROCEDURE_8:
                data = decodeString(contents);
                if (data != null && !data.equals("")) {
                    updateWorkingProcedure(data, 8);
                }
                break;
            case WORKING_PROCEDURE_9:
                data = decodeString(contents);
                if (data != null && !data.equals("")) {
                    updateWorkingProcedure(data, 9);
                }
                break;
            case WORKING_PROCEDURE_10:
                data = decodeString(contents);
                if (data != null && !data.equals("")) {
                    updateWorkingProcedure(data, 10);
                }
                break;
            case WORKING_PROCEDURE_11:
                data = decodeString(contents);
                if (data != null && !data.equals("")) {
                    updateWorkingProcedure(data, 11);
                }
                break;
            case INFO_RIGHTS:
                dataList = decodeStringList(contents);
                size = InformationRights.getInformationRightsList().size();
                if (dataList != null && dataList.size() >= (size * 2)) {
                    if (dataList.size() % 2 != 0) {
                        dataList.remove(dataList.size() - 1);
                    }
                    updateInfoRightsList(dataList);
                    updated = true;
                }
                break;
            case INFO_PROVIDING_OFFICER:
                dataList = decodeStringList(contents);
                size = InformationRights.getInformationRightsList().size();
                if (dataList != null && dataList.size() >= (size * 5)) {
                    if (dataList.size() % 5 != 0) {
                        dataList.remove(dataList.size() - 1);
                    }
                    updateInfoProvidingOfficerList(dataList);
                    updated = true;
                }
                break;
            case GIVENSE_REDRESS_SYSTEM:
                dataList = decodeStringList(contents);
                size = GivenseRedressSystem.getGivenseRedressSystemList().size();
                if (dataList != null && dataList.size() >= (size * 2)) {
                    if (dataList.size() % 2 != 0) {
                        dataList.remove(dataList.size() - 1);
                    }
                    updateGivenseRedressSystemList(dataList);
                    updated = true;
                }
                break;
            case INFO_COMMITTEE_DECISION:
                dataList = decodeStringList(contents);
                if (dataList != null && dataList.size() == 1) {
                    updateInfoCommiteeDecision(dataList.get(0));
                    updated = true;
                }
                break;
            case TENDER_NEWS:
                dataList = decodeStringList(contents);
                size = TenderNotice.getTenderNoticeList().size();
                if (dataList != null && dataList.size() >= (size * 3)) {
                    if (dataList.size() % 3 != 0) {
                        dataList.remove(dataList.size() - 1);
                    }
                    updateTenderNoticeList(dataList);
                    updated = true;
                }
                break;
            default:
                break;
        }
    }

    private void updateInfoCommiteeDecision(String s) {
        InfoCommitteeDecision infoCommitteeDecision = InfoCommitteeDecision
                .load(InfoCommitteeDecision.class, 1);
        if (infoCommitteeDecision != null) {
            Log.e(getClass().getName(), "infoCommitteeDecision retrieved");
            if (infoCommitteeDecision.getUrlLink() != null && !infoCommitteeDecision.equals(s)) {
                infoCommitteeDecision.setUrlLink(s);
                Log.e(getClass().getName(), "infoCommitteeDecision updated");
            }
        }
        infoCommitteeDecision.save();
    }

    private void updateGivenseRedressSystemList(List<String> dataList) {
        List<GivenseRedressSystem> givenseRedressSystemList = GivenseRedressSystem
                .getGivenseRedressSystemList();
        int size = givenseRedressSystemList.size();
        String name = null;
        String urlLink = null;
        int j = 0;
        if (givenseRedressSystemList != null && givenseRedressSystemList.size() > 0) {
            for (int i = 0; i < dataList.size(); i += 2) {
                Log.e(getClass().getName(), "Checking for update");
                name = dataList.get(i);
                urlLink = dataList.get(i + 1);
                if (i > size * 2) {
                    GivenseRedressSystem infgivenseRedressSystemrmationRights = new
                            GivenseRedressSystem(name, urlLink);
                    Log.e(getClass().getName(), "Givense Redress System item has been added");
                } else {
                    j++;
                    GivenseRedressSystem givenseRedressSystem = GivenseRedressSystem
                            .load(GivenseRedressSystem.class, j);
                    if (givenseRedressSystem != null) {
                        if (givenseRedressSystem.getInfo() != null &&
                                !givenseRedressSystem.getInfo()
                                        .trim()
                                        .equals(name)) {
                            givenseRedressSystem.setInfo(name);
                            Log.e(getClass().getName(), "update givense redress system List");
                        }
                        if (givenseRedressSystem.getUrlLink() != null &&
                                !givenseRedressSystem.getUrlLink()
                                        .trim()
                                        .equals(urlLink)) {
                            givenseRedressSystem.setUrlLink(urlLink);
                            Log.e(getClass().getName(), "update givense redress system List");
                        }
                        givenseRedressSystem.save();
                    }
                }
            }
        }
    }

    private void updateInfoProvidingOfficerList(List<String> dataList) {
        List<InfoProvidingOfficer> infoProvidingOfficerList = InfoProvidingOfficer.
                getInfoProvidingOfficerList();
        int size = infoProvidingOfficerList.size();
        String name = null;
        String designation = null;
        String location = null;
        String phoneNumber = null;
        String email = null;
        int j = 0;
        if (infoProvidingOfficerList != null && infoProvidingOfficerList.size() > 0) {
            for (int i = 0; i < dataList.size(); i += 5) {
                Log.e(getClass().getName(), "Checking for update");
                name = dataList.get(i);
                designation = dataList.get(i + 1);
                email = dataList.get(i + 4);
                phoneNumber = dataList.get(i + 3);
                location = dataList.get(i + 2);
                if (i > size * 5) {
                    InfoProvidingOfficer infoProvidingOfficer = new InfoProvidingOfficer
                            (name, designation, location, phoneNumber, email);
                    Log.e(getClass().getName(), "Info Providing Officer item added");
                } else {
                    j++;
                    InfoProvidingOfficer infoProvidingOfficer = InfoProvidingOfficer
                            .load(InfoProvidingOfficer.class, j);
                    if (infoProvidingOfficer != null) {
                        if (infoProvidingOfficer.getName() != null &&
                                !infoProvidingOfficer.getName()
                                        .trim()
                                        .equals(name)) {
                            infoProvidingOfficer.setName(name);
                            Log.e(getClass().getName(), "update Info Officer List");
                        }
                        if (infoProvidingOfficer.getDesignation() != null &&
                                !infoProvidingOfficer.getDesignation()
                                        .trim()
                                        .equals(designation)) {
                            infoProvidingOfficer.setDesignation(designation);
                            Log.e(getClass().getName(), "update Info Officer List");
                        }
                        if (infoProvidingOfficer.getEmail() != null &&
                                !infoProvidingOfficer.getEmail()
                                        .trim()
                                        .equals(email)) {
                            infoProvidingOfficer.setEmail(email);
                            Log.e(getClass().getName(), "update Info Officer List");
                        }
                        if (infoProvidingOfficer.getPhone() != null &&
                                !infoProvidingOfficer.getPhone()
                                        .equals(phoneNumber)) {
                            infoProvidingOfficer.setPhone(phoneNumber);
                            Log.e(getClass().getName(), "update Info Officer List");
                        }
                        if (infoProvidingOfficer.getAddress() != null &&
                                !infoProvidingOfficer.getAddress()
                                        .trim()
                                        .equals(location)) {
                            infoProvidingOfficer.setAddress(location);
                            Log.e(getClass().getName(), "update Info Officer List");
                        }
                        infoProvidingOfficer.save();
                    }
                }
            }
        }
    }

    private void updateInfoRightsList(List<String> dataList) {
        List<InformationRights> informationRightsList = InformationRights
                .getInformationRightsList();
        int size = informationRightsList.size();
        String name = null;
        String urlLink = null;
        int j = 0;
        if (informationRightsList != null && informationRightsList.size() > 0) {
            for (int i = 0; i < dataList.size(); i += 2) {
                Log.e(getClass().getName(), "Checking for update");
                name = dataList.get(i);
                urlLink = dataList.get(i + 1);
                if (i > size * 2) {
                    InformationRights informationRights = new InformationRights(name, urlLink);
                    Log.e(getClass().getName(), "Information Rights item has been added");
                } else {
                    j++;
                    InformationRights informationRights = InformationRights
                            .load(InformationRights.class, j);
                    if (informationRights != null) {
                        if (informationRights.getInfo() != null &&
                                !informationRights.getInfo()
                                        .trim()
                                        .equals(name)) {
                            informationRights.setInfo(name);
                            Log.e(getClass().getName(), "update info Rights List");
                        }
                        if (informationRights.getUrlLink() != null &&
                                !informationRights.getUrlLink()
                                        .trim()
                                        .equals(urlLink)) {
                            informationRights.setUrlLink(urlLink);
                            Log.e(getClass().getName(), "update info Rights List");
                        }
                        informationRights.save();
                    }
                }
            }
        }
    }

    private void updateWorkingProcedure(String data, int i) {
        WorkingProcedure workingProcedure = WorkingProcedure.load(WorkingProcedure.class, i);
        if (workingProcedure != null) {
            Log.e(getClass().getName(), "Working Procedure " + i + " is retrieved");
            if (workingProcedure.getDetails() != null && !workingProcedure
                    .getDetails()
                    .equals(data)) {
                workingProcedure.setDetails(data);
                Log.e(getClass().getName(), "Working Procedure " + i + " is updated");
            }
        }
        workingProcedure.save();
    }

    private void updateWorkingPeriodOfDirectiorGeneralsList(List<String> dataList) {
        List<WorkingPeriodDirectorGeneral> workingPeriodDirectorGeneralList =
                WorkingPeriodDirectorGeneral.getWorkingPeriodDtrectorGeneralList();
        int size = workingPeriodDirectorGeneralList.size();
        String name = null;
        String fromData = null;
        String toDate = null;
        int j = 0;
        if (workingPeriodDirectorGeneralList != null && workingPeriodDirectorGeneralList.size() > 0) {
            for (int i = 0; i < dataList.size(); i += 3) {
                Log.e(getClass().getName(), "Checking for update");
                name = dataList.get(i);
                fromData = dataList.get(i + 1);
                toDate = dataList.get(i + 2);
                if (i > size * 3) {
                    WorkingPeriodDirectorGeneral workingPeriodDirectorGeneral = new
                            WorkingPeriodDirectorGeneral(name, fromData, toDate);
                    Log.e(getClass().getName(), "Director General Working Period added");
                } else {
                    j++;
                    WorkingPeriodDirectorGeneral workingPeriodDirectorGeneral =
                            WorkingPeriodDirectorGeneral.load(WorkingPeriodDirectorGeneral.class, j);
                    if (workingPeriodDirectorGeneral != null) {
                        if (workingPeriodDirectorGeneral.getName() != null &&
                                !workingPeriodDirectorGeneral.getName()
                                        .trim()
                                        .equals(name)) {
                            workingPeriodDirectorGeneral.setName(name);
                            Log.e(getClass().getName(), "Director General Working Period updated");
                        }
                        if (workingPeriodDirectorGeneral.getFromDate() != null &&
                                !workingPeriodDirectorGeneral.getFromDate()
                                        .trim()
                                        .equals(fromData)) {
                            workingPeriodDirectorGeneral.setFromDate(fromData);
                            Log.e(getClass().getName(), "Director General Working Period updated");
                        }
                        if (workingPeriodDirectorGeneral.getToDate() != null &&
                                !workingPeriodDirectorGeneral.getToDate()
                                        .trim()
                                        .equals(toDate)) {
                            workingPeriodDirectorGeneral.setToDate(toDate);
                            Log.e(getClass().getName(), "Director General Working Period updated");
                        }
                        workingPeriodDirectorGeneral.save();
                    }
                }
            }
        }

    }

    private void updateFuturePlan(String data) {
        FuturePlan futurePlan = FuturePlan.load(FuturePlan.class, 1);
        if (futurePlan != null) {
            Log.e(getClass().getName(), "Retrieved Future Plan");
            if (futurePlan.getDetails() != null && !futurePlan
                    .getDetails()
                    .trim()
                    .equals(data)) {
                futurePlan.setDetails(data);
                Log.e(getClass().getName(), "Update Future Plan");
            }
        }
        futurePlan.save();
    }

    private void updateHistory(String data) {
        History history = History.load(History.class, 1);
        if (history != null) {
            Log.e(getClass().getName(), "Retrieved History");
            if (history.getDetails() != null && !history
                    .getDetails()
                    .trim()
                    .equals(data)) {
                history.setDetails(data);
                Log.e(getClass().getName(), "Update History");
            }
        }
        history.save();
    }

    private String decodeString(String content) {
        String res = null;
        if (content != null) {
            res = "";
            boolean nextStep = false;
            for (int i = 0; i < content.length(); i++) {
                if (content.charAt(i) == '<' || content.charAt(i) == '&') {
                    nextStep = true;
                    continue;
                } else if (content.charAt(i) == '>') {
                    nextStep = false;
                    continue;
                } else if (content.charAt(i) == ';') {
                    nextStep = false;
                    res = res + Character.toString(' ');
                    continue;
                } else if ((content.charAt(i) >= 'A' && content.charAt(i) <= 'Z') ||
                        (content.charAt(i) >= 'a' && content.charAt(i) <= 'z') ||
                        (content.charAt(i) >= '0' && content.charAt(i) <= '9') ||
                        content.charAt(i) == '-' || content.charAt(i) == '"' ||
                        content.charAt(i) == ':') {
                    continue;
                } else {
                    if (!nextStep) {
                        if (content.charAt(i) == '|') {
                            res = res + Character.toString('।');
                        } else {
                            res = res + Character.toString(content.charAt(i));
                        }
                    }
                }
            }
        }
        return res;
    }

    private void updatePrimaryEducationDirectorirate(List<String> dataList) {
        PrimaryEducationDepartmentInfo primaryEducationDepartmentInfo =
                PrimaryEducationDepartmentInfo
                        .load(PrimaryEducationDepartmentInfo.class, 1);
        if (primaryEducationDepartmentInfo != null) {
            Log.e(getClass().getName(), "retrieved primary education info");
            String location = dataList.get(0);
            String phone = dataList.get(1);
            String fax = dataList.get(2);
            String email = dataList.get(3);
            double latitude = Double.parseDouble(dataList.get(4));
            double longtitude = Double.parseDouble(dataList.get(5));
            String imageLink = dataList.get(6);
            if (primaryEducationDepartmentInfo.getAddress() != null &&
                    !primaryEducationDepartmentInfo.getAddress()
                            .trim()
                            .equals(location)) {
                primaryEducationDepartmentInfo.setAddress(location);
                Log.e(getClass().getName(), "update primary education info");
            }
            if (primaryEducationDepartmentInfo.getPhone() != null &&
                    !primaryEducationDepartmentInfo.getPhone()
                            .trim()
                            .equals(phone)) {
                primaryEducationDepartmentInfo.setPhone(phone);
                Log.e(getClass().getName(), "update primary education info");
            }
            if (primaryEducationDepartmentInfo.getEmail() != null &&
                    !primaryEducationDepartmentInfo.getEmail()
                            .trim()
                            .equals(email)) {
                primaryEducationDepartmentInfo.setEmail(email);
                Log.e(getClass().getName(), "update primary education info");
            }
            if (primaryEducationDepartmentInfo.getFax() != null &&
                    !primaryEducationDepartmentInfo.getFax()
                            .equals(fax)) {
                primaryEducationDepartmentInfo.setFax(fax);
                Log.e(getClass().getName(), "update primary education info");
            }
            if (primaryEducationDepartmentInfo.getImageLink() != null &&
                    !primaryEducationDepartmentInfo.getImageLink()
                            .trim()
                            .equals(imageLink)) {
                primaryEducationDepartmentInfo.setImageLink(imageLink);
                Log.e(getClass().getName(), "update primary education info");
            }
            if (primaryEducationDepartmentInfo.getLatitude() != latitude) {
                primaryEducationDepartmentInfo.setLatitude(latitude);
                Log.e(getClass().getName(), "update primary education info");
            }

            if (primaryEducationDepartmentInfo.getLongitude() != longtitude) {
                primaryEducationDepartmentInfo.setLatitude(longtitude);
                Log.e(getClass().getName(), "update primary education info");
            }

        }
        primaryEducationDepartmentInfo.save();
    }

    private void updateAdvertisementList(List<String> dataList) {
        List<Advertisement> advertisementList = Advertisement.getAdvertisementList();
        int size = advertisementList.size();
        String name = null;
        String urlLink = null;
        int j = 0;
        if (advertisementList != null && advertisementList.size() > 0) {
            for (int i = 0; i < dataList.size(); i += 2) {
                Log.e(getClass().getName(), "Checking for update");
                name = dataList.get(i);
                urlLink = dataList.get(i + 1);
                if (i > size * 2) {
                    Advertisement advertisement = new Advertisement(name, urlLink);
                    Log.e(getClass().getName(), "Advertisement item added");
                } else {
                    j++;
                    Advertisement advertisement = Advertisement.load(Advertisement.class, j);
                    if (advertisement != null) {
                        if (advertisement.getRulsName() != null &&
                                !advertisement.getRulsName()
                                        .trim()
                                        .equals(name)) {
                            advertisement.setRulsName(name);
                            Log.e(getClass().getName(), "update advertisement List");
                        }
                        if (advertisement.getUrlLink() != null &&
                                !advertisement.getUrlLink()
                                        .trim()
                                        .equals(urlLink)) {
                            advertisement.setUrlLink(urlLink);
                            Log.e(getClass().getName(), "update advertisement List");
                        }
                        advertisement.save();

                    }
                }
            }
        }
    }

    private void updateRulesList(List<String> dataList) {
        List<Rules> rulesList = Rules.getRulesList();
        int size = rulesList.size();
        String name = null;
        String urlLink = null;
        int j = 0;
        if (rulesList != null && rulesList.size() > 0) {
            for (int i = 0; i < dataList.size(); i += 2) {
                Log.e(getClass().getName(), "Checking for update");
                name = dataList.get(i);
                urlLink = dataList.get(i + 1);
                if (i > size * 2) {
                    Rules rules = new Rules(name, urlLink);
                    Log.e(getClass().getName(), "Rules item added");
                } else {
                    j++;
                    Rules rules = Rules.load(Rules.class, j);
                    if (rules != null) {
                        if (rules.getRulsName() != null &&
                                !rules.getRulsName()
                                        .trim()
                                        .equals(name)) {
                            rules.setRulsName(name);
                            Log.e(getClass().getName(), "update rules List");
                        }
                        if (rules.getUrlLink() != null &&
                                !rules.getUrlLink()
                                        .trim()
                                        .equals(urlLink)) {
                            rules.setUrlLink(urlLink);
                            Log.e(getClass().getName(), "update rules List");
                        }
                        rules.save();

                    }
                }
            }
        }
    }

    private void updateEPrimarySystemList(List<String> dataList) {
        List<E_PrimarySystem> e_primarySystemList = E_PrimarySystem.getE_primarySystemList();
        int size = e_primarySystemList.size();
        String name = null;
        String urlLink = null;
        int j = 0;
        if (e_primarySystemList != null && e_primarySystemList.size() > 0) {
            for (int i = 0; i < dataList.size(); i += 2) {
                Log.e(getClass().getName(), "Checking for update");
                name = dataList.get(i);
                urlLink = dataList.get(i + 1);
                if (i > size * 2) {
                    E_PrimarySystem e_primarySystem = new E_PrimarySystem(name, urlLink);
                    Log.e(getClass().getName(), "E Primary System item added");
                } else {
                    j++;
                    E_PrimarySystem e_primarySystem = E_PrimarySystem
                            .load(E_PrimarySystem.class, j);
                    if (e_primarySystem != null) {
                        if (e_primarySystem.getName() != null &&
                                !e_primarySystem.getName()
                                        .trim()
                                        .equals(name)) {
                            e_primarySystem.setName(name);
                            Log.e(getClass().getName(), "e primary system List");
                        }
                        if (e_primarySystem.getUrlLink() != null &&
                                !e_primarySystem.getUrlLink()
                                        .trim()
                                        .equals(urlLink)) {
                            e_primarySystem.setUrlLink(urlLink);
                            Log.e(getClass().getName(), "e primary system List");
                        }
                        e_primarySystem.save();

                    }
                }
            }
        }
    }


    private void updatePtiOfficeList(List<String> dataList) {
        List<PtiOffice> ptiOfficeList = PtiOffice.getPtiOfficeList();
        int size = ptiOfficeList.size();
        String name = null;
        String designation = null;
        String location = null;
        String phoneNumber = null;
        String fax = null;
        String email = null;
        int j = 0;
        if (ptiOfficeList != null && ptiOfficeList.size() > 0) {
            for (int i = 0; i < dataList.size(); i += 6) {
                Log.e(getClass().getName(), "Checking for update");
                name = dataList.get(i);
                designation = dataList.get(i + 1);
                location = dataList.get(i + 2);
                email = dataList.get(i + 3);
                phoneNumber = dataList.get(i + 4);
                fax = dataList.get(i + 5);
                if (i > size * 6) {
                    PtiOffice ptiOffice = new PtiOffice(name, designation,
                            email, phoneNumber, fax, location);
                    Log.e(getClass().getName(), "Pti Office item added");
                } else {
                    j++;
                    PtiOffice ptiOffice = DistrictOffice.load(DistrictOffice.class, j);
                    if (ptiOffice != null) {
                        if (ptiOffice.getName() != null &&
                                !ptiOffice.getName()
                                        .trim()
                                        .equals(name)) {
                            ptiOffice.setName(name);
                            Log.e(getClass().getName(), "update Pti Office List");
                        }
                        if (ptiOffice.getDesignation() != null &&
                                !ptiOffice.getDesignation()
                                        .trim()
                                        .equals(designation)) {
                            ptiOffice.setDesignation(designation);
                            Log.e(getClass().getName(), "update Pti Office List");
                        }
                        if (ptiOffice.getEmail() != null &&
                                !ptiOffice.getEmail()
                                        .trim()
                                        .equals(email)) {
                            ptiOffice.setEmail(email);
                            Log.e(getClass().getName(), "update Pti Office List");
                        }
                        if (ptiOffice.getTelephone() != null &&
                                !ptiOffice.getTelephone()
                                        .equals(phoneNumber)) {
                            ptiOffice.setTelephone(phoneNumber);
                            Log.e(getClass().getName(), "update Pti Office List");
                        }
                        if (ptiOffice.getFax() != null &&
                                !ptiOffice.getFax()
                                        .trim()
                                        .equals(fax)) {
                            ptiOffice.setFax(fax);
                            Log.e(getClass().getName(), "update Pti Office List");
                        }
                        if (ptiOffice.getDistrict() != null &&
                                !ptiOffice.getDistrict()
                                        .trim()
                                        .equals(location)) {
                            ptiOffice.setDistrict(location);
                            Log.e(getClass().getName(), "update Pti Office List");
                        }
                        ptiOffice.save();
                    }
                }
            }
        }
    }

    private void updateDistrictOfficeList(List<String> dataList) {
        List<DistrictOffice> districtOfficeList = DistrictOffice.getDistrictOfficeList();
        int size = districtOfficeList.size();
        String name = null;
        String designation = null;
        String location = null;
        String phoneNumber = null;
        String fax = null;
        String email = null;
        int j = 0;
        if (districtOfficeList != null && districtOfficeList.size() > 0) {
            for (int i = 0; i < dataList.size(); i += 6) {
                Log.e(getClass().getName(), "Checking for update");
                name = dataList.get(i);
                designation = dataList.get(i + 1);
                location = dataList.get(i + 2);
                email = dataList.get(i + 3);
                phoneNumber = dataList.get(i + 4);
                fax = dataList.get(i + 5);
                if (i > size * 6) {
                    DistrictOffice districtOffice = new DistrictOffice(name, designation,
                            email, phoneNumber, fax, location);
                    Log.e(getClass().getName(), "District Office item added");
                } else {
                    j++;
                    DistrictOffice districtOffice = DistrictOffice.load(DistrictOffice.class, j);
                    if (districtOffice != null) {
                        if (districtOffice.getName() != null &&
                                !districtOffice.getName()
                                        .trim()
                                        .equals(name)) {
                            districtOffice.setName(name);
                            Log.e(getClass().getName(), "update District Office List");
                        }
                        if (districtOffice.getDesignation() != null &&
                                !districtOffice.getDesignation()
                                        .trim()
                                        .equals(designation)) {
                            districtOffice.setDesignation(designation);
                            Log.e(getClass().getName(), "update District Office List");
                        }
                        if (districtOffice.getEmail() != null &&
                                !districtOffice.getEmail()
                                        .trim()
                                        .equals(email)) {
                            districtOffice.setEmail(email);
                            Log.e(getClass().getName(), "update District Office List");
                        }
                        if (districtOffice.getTelephone() != null &&
                                !districtOffice.getTelephone()
                                        .equals(phoneNumber)) {
                            districtOffice.setTelephone(phoneNumber);
                            Log.e(getClass().getName(), "update District Office List");
                        }
                        if (districtOffice.getFax() != null &&
                                !districtOffice.getFax()
                                        .trim()
                                        .equals(fax)) {
                            districtOffice.setFax(fax);
                            Log.e(getClass().getName(), "update District Office List");
                        }
                        if (districtOffice.getDistrict() != null &&
                                !districtOffice.getDistrict()
                                        .trim()
                                        .equals(location)) {
                            districtOffice.setDistrict(location);
                            Log.e(getClass().getName(), "update District Office List");
                        }
                        districtOffice.save();
                    }
                }
            }
        }
    }

    private void updateDivisionList(List<String> dataList) {
        List<DivisionOffice> divisionOfficeList = DivisionOffice.getDivisionOfficeList();
        int size = divisionOfficeList.size();
        String name = null;
        String designation = null;
        String location = null;
        String phoneNumber = null;
        String fax = null;
        String email = null;
        int j = 0;
        if (divisionOfficeList != null && divisionOfficeList.size() > 0) {
            for (int i = 0; i < dataList.size(); i += 6) {
                Log.e(getClass().getName(), "Checking for update");
                name = dataList.get(i);
                designation = dataList.get(i + 1);
                email = dataList.get(i + 2);
                phoneNumber = dataList.get(i + 3);
                fax = dataList.get(i + 4);
                location = dataList.get(i + 5);
                if (i > size * 6) {
                    DivisionOffice divisionOffice = new DivisionOffice(name, designation,
                            email, phoneNumber, fax, location);
                    Log.e(getClass().getName(), "Division Office item added");
                } else {
                    j++;
                    DivisionOffice divisionOffice = DivisionOffice.load(DivisionOffice.class, j);
                    if (divisionOffice != null) {
                        if (divisionOffice.getName() != null &&
                                !divisionOffice.getName()
                                        .trim()
                                        .equals(name)) {
                            divisionOffice.setName(name);
                            Log.e(getClass().getName(), "update Division Office List");
                        }
                        if (divisionOffice.getDesignation() != null &&
                                !divisionOffice.getDesignation()
                                        .trim()
                                        .equals(designation)) {
                            divisionOffice.setDesignation(designation);
                            Log.e(getClass().getName(), "update Division Office List");
                        }
                        if (divisionOffice.getEmail() != null &&
                                !divisionOffice.getEmail()
                                        .trim()
                                        .equals(email)) {
                            divisionOffice.setEmail(email);
                            Log.e(getClass().getName(), "update Division Office List");
                        }
                        if (divisionOffice.getTelephone() != null &&
                                !divisionOffice.getTelephone()
                                        .equals(phoneNumber)) {
                            divisionOffice.setTelephone(phoneNumber);
                            Log.e(getClass().getName(), "update Division Office List");
                        }
                        if (divisionOffice.getFax() != null &&
                                !divisionOffice.getFax()
                                        .trim()
                                        .equals(fax)) {
                            divisionOffice.setFax(fax);
                            Log.e(getClass().getName(), "update Division Office List");
                        }
                        if (divisionOffice.getDivision() != null &&
                                !divisionOffice.getDivision()
                                        .trim()
                                        .equals(location)) {
                            divisionOffice.setDivision(location);
                            Log.e(getClass().getName(), "update Division Office List");
                        }
                        divisionOffice.save();
                    }
                }
            }
        }
    }

    private void updateFormsList(List<String> dataList) {
        List<Forms> formsList = Forms.getFormsList();
        int size = formsList.size();
        String name = null;
        String urlLink = null;
        int j = 0;
        if (formsList != null && formsList.size() > 0) {
            for (int i = 0; i < dataList.size(); i += 2) {
                Log.e(getClass().getName(), "Checking for update");
                name = dataList.get(i);
                urlLink = dataList.get(i + 1);
                if (i > size * 2) {
                    Forms forms = new Forms(name, urlLink);
                    Log.e(getClass().getName(), "Forms Result item added");
                } else {
                    j++;
                    Forms forms = Forms.load(Forms.class, j);
                    if (forms.getRules() != null &&
                            !forms.getRules()
                                    .trim()
                                    .equals(name)) {
                        forms.setRules(name);
                        Log.e(getClass().getName(), "update forms Link List");
                    }
                    if (forms.getUrlLink() != null &&
                            !forms.getUrlLink()
                                    .trim()
                                    .equals(urlLink)) {
                        forms.setUrlLink(urlLink);
                        Log.e(getClass().getName(), "update forms Link List");
                    }
                    forms.save();

                }
            }
        }
    }

    private void updatePublicationList(List<String> dataList) {
        List<MonthlyInvestigation> monthlyInvestigationList =
                MonthlyInvestigation.getMonthlyInvestigationList();
        int size = monthlyInvestigationList.size();
        String name = null;
        String urlLink = null;
        int j = 0;
        if (monthlyInvestigationList != null && monthlyInvestigationList.size() > 0) {
            for (int i = 0; i < dataList.size(); i += 2) {
                Log.e(getClass().getName(), "Checking for update");
                name = dataList.get(i);
                urlLink = dataList.get(i + 1);
                if (i > size * 2) {
                    MonthlyInvestigation monthlyInvestigations = new
                            MonthlyInvestigation(name, urlLink);
                    Log.e(getClass().getName(), "publications Result item added");
                } else {
                    j++;
                    MonthlyInvestigation monthlyInvestigation =
                            MonthlyInvestigation.load(MonthlyInvestigation.class, j);
                    if (monthlyInvestigation != null) {
                        if (monthlyInvestigation.getRules() != null &&
                                !monthlyInvestigation
                                        .getRules()
                                        .trim()
                                        .equals(name)) {
                            monthlyInvestigation.setRules(name);
                            Log.e(getClass().getName(), "update monthly investigation List");
                        }
                        if (monthlyInvestigation.getUrlLink() != null &&
                                !monthlyInvestigation
                                        .getUrlLink()
                                        .trim()
                                        .equals(urlLink)) {
                            monthlyInvestigation.setUrlLink(urlLink);
                            Log.e(getClass().getName(), "update monthly investigation List");
                        }
                        monthlyInvestigation.save();
                    }

                }
            }
        }
    }

    private void updateMOnthlyInvestigation(List<String> dataList) {
        List<MonthlyInvestigation> monthlyInvestigationList =
                MonthlyInvestigation.getMonthlyInvestigationList();
        int size = monthlyInvestigationList.size();
        String name = null;
        String urlLink = null;
        int j = 0;
        if (monthlyInvestigationList != null && monthlyInvestigationList.size() > 0) {
            for (int i = 0; i < dataList.size(); i += 2) {
                Log.e(getClass().getName(), "Checking for update");
                name = dataList.get(i);
                urlLink = dataList.get(i + 1);
                if (i > size * 2) {
                    MonthlyInvestigation monthlyInvestigations = new
                            MonthlyInvestigation(name, urlLink);
                    Log.e(getClass().getName(), "Monthly Investigation Result item added");
                } else {
                    j++;
                    MonthlyInvestigation monthlyInvestigation =
                            MonthlyInvestigation.load(MonthlyInvestigation.class, j);
                    if (monthlyInvestigation.getRules() != null &&
                            !monthlyInvestigation
                                    .getRules()
                                    .trim()
                                    .equals(name)) {
                        monthlyInvestigation.setRules(name);
                        Log.e(getClass().getName(), "update monthly Investigation Result Link List");
                    }
                    if (monthlyInvestigation.getUrlLink() != null &&
                            !monthlyInvestigation
                                    .getUrlLink()
                                    .trim()
                                    .equals(urlLink)) {
                        monthlyInvestigation.setUrlLink(urlLink);
                        Log.e(getClass().getName(), "update monthly Investigation Result Link List");
                    }
                    monthlyInvestigation.save();

                }
            }
        }
    }

    private void updateMadrashaResultStatictics(List<String> dataList) {
        List<MadrashaResultStatictics> madrashaResultStaticticsList =
                MadrashaResultStatictics.getMadrashaResultStaticticsList();
        int size = madrashaResultStaticticsList.size();
        String name = null;
        String urlLink = null;
        int j = 0;
        if (madrashaResultStaticticsList != null && madrashaResultStaticticsList.size() > 0) {
            for (int i = 0; i < dataList.size(); i += 2) {
                Log.e(getClass().getName(), "Checking for update");
                name = dataList.get(i);
                urlLink = dataList.get(i + 1);
                if (i > size * 2) {
                    MadrashaResultStatictics madrashaResultStatictics = new
                            MadrashaResultStatictics(name, urlLink);
                    Log.e(getClass().getName(), "Madrasha Statictics Result item added");
                } else {
                    j++;
                    MadrashaResultStatictics madrashaResultStatictics =
                            MadrashaResultStatictics.load(MadrashaResultStatictics.class, j);
                    if (madrashaResultStatictics != null) {
                        if (madrashaResultStatictics.getRules() != null &&
                                !madrashaResultStatictics
                                        .getRules()
                                        .trim()
                                        .equals(name)) {
                            madrashaResultStatictics.setRules(name);
                            Log.e(getClass().getName(), "update madrasha Result Link List");
                        }
                        if (madrashaResultStatictics.getUrlLink() != null &&
                                !madrashaResultStatictics
                                        .getUrlLink()
                                        .trim()
                                        .equals(urlLink)) {
                            madrashaResultStatictics.setUrlLink(urlLink);
                            Log.e(getClass().getName(), "update madrasha Result Link List");
                        }
                        madrashaResultStatictics.save();

                    }
                }
            }
        }
    }

    private void updateSchoolResultStatictics(List<String> dataList) {
        List<SchoolResultStatictics> schoolResultStaticticsList =
                SchoolResultStatictics.getSchoolResultStaticticsList();
        int size = schoolResultStaticticsList.size();
        String name = null;
        String urlLink = null;
        int j = 0;
        if (schoolResultStaticticsList != null && schoolResultStaticticsList.size() > 0) {
            for (int i = 0; i < dataList.size(); i += 2) {
                Log.e(getClass().getName(), "Checking for update");
                name = dataList.get(i);
                urlLink = dataList.get(i + 1);
                if (i > size * 2) {
                    SchoolResultStatictics schoolResultStatictics = new SchoolResultStatictics
                            (name, urlLink);
                    Log.e(getClass().getName(), "School Statictics Result item added");
                } else {
                    j++;
                    SchoolResultStatictics schoolResultStatictics =
                            SchoolResultStatictics.load(SchoolResultStatictics.class, j);
                    if (schoolResultStatictics != null) {
                        if (schoolResultStatictics.getRules() != null &&
                                !schoolResultStatictics
                                        .getRules()
                                        .trim()
                                        .equals(name)) {
                            schoolResultStatictics.setRules(name);
                            Log.e(getClass().getName(), "update School Result Link List");
                        }
                        if (schoolResultStatictics.getUrlLink() != null &&
                                !schoolResultStatictics
                                        .getUrlLink()
                                        .trim()
                                        .equals(urlLink)) {
                            schoolResultStatictics.setUrlLink(urlLink);
                            Log.e(getClass().getName(), "update School Result Link List");
                        }
                        schoolResultStatictics.save();
                    }
                }
            }
        }
    }

    private void updateProjectPEDP3(List<String> dataList) {
        List<ProjectPedp3> projectPedp3List =
                ProjectPedp3.getProjectPedp3List();
        int size = projectPedp3List.size();
        String name = null;
        String urlLink = null;
        int j = 0;
        if (projectPedp3List != null && projectPedp3List.size() > 0) {
            for (int i = 0; i < dataList.size(); i += 2) {
                Log.e(getClass().getName(), "Checking for update");
                name = dataList.get(i);
                urlLink = dataList.get(i + 1);
                if (i > size * 2) {
                    ProjectPedp3 projectPedp3 = new ProjectPedp3(name, urlLink);
                    Log.e(getClass().getName(), "Project Pedp 3item added");
                } else {
                    j++;
                    ProjectPedp3 projectPedp3 =
                            ProjectPedp3.load(ProjectPedp3.class, j);
                    if (projectPedp3.getRules() != null &&
                            !projectPedp3
                                    .getRules()
                                    .trim()
                                    .equals(name)) {
                        projectPedp3.setRules(name);
                        Log.e(getClass().getName(), "Project Pedp 3 Link List");
                    }
                    if (projectPedp3.getUrlLink() != null &&
                            !projectPedp3
                                    .getUrlLink()
                                    .trim()
                                    .equals(urlLink)) {
                        projectPedp3.setUrlLink(urlLink);
                        Log.e(getClass().getName(), "Project Pedp 3 Link List");
                    }
                    projectPedp3.save();

                }
            }
        }
    }


    private void updateTenderNoticeList(List<String> dataList) {
        List<TenderNotice> tenderNoticeList =
                TenderNotice.getTenderNoticeList();
        int size = tenderNoticeList.size();
        String name = null;
        String urlLink = null;
        String date = null;
        int j = 0;
        if (tenderNoticeList != null && tenderNoticeList.size() > 0) {
            for (int i = 0; i < dataList.size(); i += 3) {
                Log.e(getClass().getName(), "Checking for update");
                name = dataList.get(i);
                date = dataList.get(i + 1);
                urlLink = dataList.get(i + 2);
                if (i > size * 3) {
                    TenderNotice tenderNotice = new TenderNotice(name, date, urlLink);
                    Log.e(getClass().getName(), "Tender item added");
                } else {
                    j++;
                    TenderNotice tenderNotice =
                            TenderNotice.load(TenderNotice.class, j);
                    if (tenderNotice != null) {
                        if (tenderNotice.getTenderName() != null &&
                                !tenderNotice
                                        .getTenderName()
                                        .trim()
                                        .equals(name)) {
                            tenderNotice.setTenderName(name);
                            Log.e(getClass().getName(), "Updated Tender List");
                        }

                        if (tenderNotice.getDate() != null &&
                                !tenderNotice
                                        .getDate()
                                        .trim()
                                        .equals(name)) {
                            tenderNotice.setDate(date);
                            Log.e(getClass().getName(), "Updated Tender List");
                        }
                        if (tenderNotice.getUrlLink() != null &&
                                !tenderNotice
                                        .getUrlLink()
                                        .trim()
                                        .equals(urlLink)) {
                            tenderNotice.setUrlLink(urlLink);
                            Log.e(getClass().getName(), "Updated Tender List");
                        }
                        tenderNotice.save();
                    }
                }
            }
        }
    }

    private void updateDirectorGeneral(List<String> dataList) {
        DirectorGeneral directorGeneral = DirectorGeneral.load(DirectorGeneral.class, 1);
        String imageLink = dataList.get(0);
        String details = dataList.get(1);
        if (directorGeneral != null) {
            Log.e(getClass().getName(), "Director General Has been retrieved");
            if (imageLink != null && !imageLink.equals(directorGeneral.getImageLink()) &&
                    !imageLink.equals("") && !imageLink.equals("null")) {
                directorGeneral.setImageLink(imageLink);
                Log.e(getClass().getName(), "Director General Has been updated");
            }
            if (details != null && !details.equals(directorGeneral.getDetails()) &&
                    !details.equals("") && !details.equals("null")) {
                directorGeneral.setDetails(details);
                Log.e(getClass().getName(), "Director General Has been updated");
            }
            directorGeneral.save();
        }
    }

    private void updateImportantLinkList(List<String> dataList) {
        List<ImportantLink> importantLinkList =
                ImportantLink.getImportantLinkList();
        int size = importantLinkList.size();
        String name = null;
        String urlLink = null;
        int j = 0;
        if (importantLinkList != null && importantLinkList.size() > 0) {
            for (int i = 0; i < dataList.size(); i += 2) {
                Log.e(getClass().getName(), "Checking for update");
                name = dataList.get(i);
                urlLink = dataList.get(i + 1);
                if (i > size * 2) {
                    ImportantLink importantLink = new ImportantLink(name, urlLink);
                    Log.e(getClass().getName(), "Important Link item added");
                } else {
                    j++;
                    ImportantLink importantLink =
                            ImportantLink.load(ImportantLink.class, j);
                    if (importantLink.getRulsName() != null &&
                            !importantLink
                                    .getRulsName()
                                    .trim()
                                    .equals(name)) {
                        importantLink.setRulsName(name);
                        Log.e(getClass().getName(), "Updated Important Link List");
                    }
                    if (importantLink.getUrlLink() != null &&
                            !importantLink
                                    .getUrlLink()
                                    .trim()
                                    .equals(urlLink)) {
                        importantLink.setUrlLink(urlLink);
                        Log.e(getClass().getName(), "Updated Important Link List");
                    }
                    importantLink.save();

                }
            }
        }
    }


    private void updateInternalServicesList(List<String> dataList) {
        List<InternalServices> internalServicesList =
                InternalServices.getInternalServicesList();
        int size = internalServicesList.size();
        String name = null;
        String urlLink = null;
        int j = 0;
        if (internalServicesList != null && internalServicesList.size() > 0) {
            for (int i = 0; i < dataList.size(); i += 2) {
                Log.e(getClass().getName(), "Checking for update");
                name = dataList.get(i);
                urlLink = dataList.get(i + 1);
                if (i > size * 2) {
                    InternalServices internalServices = new InternalServices(name, urlLink);
                    Log.e(getClass().getName(), "Internal Services item added");
                } else {
                    j++;
                    InternalServices internalServices =
                            InternalServices.load(InternalServices.class, j);
                    if (internalServices.getRulsName() != null &&
                            !internalServices
                                    .getRulsName()
                                    .trim()
                                    .equals(name)) {
                        internalServices.setRulsName(name);
                        Log.e(getClass().getName(), "Updated Internal Services List");
                    }
                    if (internalServices.getUrlLink() != null &&
                            !internalServices
                                    .getUrlLink()
                                    .trim()
                                    .equals(urlLink)) {
                        internalServices.setUrlLink(urlLink);
                        Log.e(getClass().getName(), "Updated Internal Services List");
                    }
                    internalServices.save();

                }
            }
        }
    }

    private void updateCentralEServicesList(List<String> dataList) {
        List<CentralEServices> centralEServicesList =
                CentralEServices.getCentralEServicesList();
        int size = centralEServicesList.size();
        String name = null;
        String urlLink = null;
        int j = 0;
        if (centralEServicesList != null && centralEServicesList.size() > 0) {
            for (int i = 0; i < dataList.size(); i += 2) {
                Log.e(getClass().getName(), "Checking for update");
                name = dataList.get(i);
                urlLink = dataList.get(i + 1);
                if (i > size * 2) {
                    CentralEServices centralEServices = new CentralEServices(name, urlLink);
                    Log.e(getClass().getName(), "Central E Services item added");
                } else {
                    j++;
                    CentralEServices centralEServices =
                            CentralEServices.load(CentralEServices.class, j);
                    if (centralEServices.getRulsName() != null &&
                            !centralEServices
                                    .getRulsName()
                                    .trim()
                                    .equals(name)) {
                        centralEServices.setRulsName(name);
                        Log.e(getClass().getName(), "Updated Central E Services List");
                    }
                    if (centralEServices.getUrlLink() != null &&
                            !centralEServices
                                    .getUrlLink()
                                    .trim()
                                    .equals(urlLink)) {
                        centralEServices.setUrlLink(urlLink);
                        Log.e(getClass().getName(), "Updated Central E Services List");
                    }
                    centralEServices.save();

                }
            }
        }
    }

    private List<String> decodeStringList(String content) {
        List<String> stringList = new ArrayList<String>();
        int count = 0;
        Boolean skip = true;
        char character;
        StringBuffer info = null;
        //Log.e(getClass().getName(), content);
        for (int i = 0; i < content.length(); i++) {
            character = content.charAt(i);
            if (character == '<' || character == '&') {
                skip = true;
                continue;
            } else if (character == '>') {
                skip = false;
                if (info != null && !info.toString().trim().equals("")) {
                    Log.e(getClass().getName(), count + " " + info.toString());
                    stringList.add(info.toString().trim());
                    //Log.e(getClass().getName(), "Gone");
                }
                count++;
                info = new StringBuffer();
                info.append("");
                continue;
            } else if (character == ';') {
                skip = false;
                continue;
            }
            if (!skip) {
                if (character == '|') {
                    info.append(Character.toString('।'));
                } else {
                    info.append(Character.toString(character));
                }
            }
        }
        return stringList;
    }


    private void getImageUpdate() {
        try {
            JSONObject jsonput = new JSONObject();
            JSONObject jsonsend = null;
            jsonput.put(JsonFields.app_id, APP_ID);
            jsonsend = JsonPostClient.SendHttpPost(JsonLinks.getGallery,
                    jsonput);
            if (jsonsend != null) {
                String jsonData = jsonsend.toString();
                Log.e(getClass().getName(), jsonsend.toString());
                if (jsonData != null && !jsonData.equals("")) {
                    Gson gson = new Gson();
                    GalleryImages galleryImages = gson.fromJson(jsonData, GalleryImages.class);
                    if (galleryImages != null) {
                        ArrayList<GalleryImageContents> galleryImageContentsArrayList =
                                galleryImages.getImageContentsArrayList();
                        if (galleryImageContentsArrayList != null
                                && galleryImageContentsArrayList.size() > 0) {
                            Log.e(getClass().getName(), "Gallery Images Retreived");
                            getAllImageArray(galleryImageContentsArrayList);

                        }
                    }
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void getAllImageArray(ArrayList<GalleryImageContents>
                                          galleryImageContentsArrayList) {
        List<PhotoGallery> imageGalleryTableList = PhotoGallery
                .photoGalleryList();
        if (imageGalleryTableList != null && imageGalleryTableList.size() > 0) {
            for (int i = 0; i < galleryImageContentsArrayList.size(); i++) {
                GalleryImageContents galleryImageContents =
                        galleryImageContentsArrayList.get(i);
                String active = galleryImageContents.getActive();
                String description = galleryImageContents.getDescription();
                String filePath = galleryImageContents.getImagePath();
                Log.e(getClass().getName(), "Image reterived");
                updated = true;
                if (active.equals("1")) {
                    if (i >= imageGalleryTableList.size()) {
                        PhotoGallery imageGalleryTable = new
                                PhotoGallery(filePath, description);
                        Log.e(getClass().getName(), "Image Added");

                    } else {
                        PhotoGallery imageGalleryTable = PhotoGallery
                                .load(PhotoGallery.class, imageGalleryTableList.get(i).getId());
                        if (imageGalleryTable != null) {
                            if (imageGalleryTable.getDescription() != null &&
                                    !imageGalleryTable.getDescription().equals(description)) {
                                imageGalleryTable.setDescription(description);
                                Log.e(getClass().getName(), "updated image");
                            }
                            if (imageGalleryTable.getImageLink() != null &&
                                    !imageGalleryTable.getImageLink().equals(filePath)) {
                                imageGalleryTable.setImageLink(filePath);
                                Log.e(getClass().getName(), "updated image");
                            }


                        }
                        imageGalleryTable.save();

                    }
                }

            }
        }
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        SweetAlertDialog sweetAlertDialog = null;

        if (aBoolean) {
            sweetAlertDialog = new SweetAlertDialog(context,
                    SweetAlertDialog.SUCCESS_TYPE);
            sweetAlertDialog.setTitleText(context.getResources()
                    .getString(R.string.updateSuccessfulNotifier));

        } else {
            sweetAlertDialog = new SweetAlertDialog(context,
                    SweetAlertDialog.ERROR_TYPE);
            sweetAlertDialog.setTitleText(context.getResources()
                    .getString(R.string.updateFailedNotifier));

        }
        if (sweetAlertDialog != null) {
            sweetAlertDialog.setConfirmText("বন্ধ করুন");
            sweetAlertDialog.showCancelButton(false);
            sweetAlertDialog.setCancelable(false);
            sweetAlertDialog.setConfirmClickListener
                    (new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismissWithAnimation();
                        }
                    });
            sweetAlertDialog.show();
        }


    }

    private void getMenuUpdate() {

        try {
            JSONObject jsonput = new JSONObject();
            JSONObject jsonsend = null;
            jsonput.put(JsonFields.app_id, APP_ID);
            jsonsend = JsonPostClient.SendHttpPost(JsonLinks.menuList,
                    jsonput);
            if (jsonsend != null) {
                String jsonString = jsonsend.toString();
                if (jsonString != null && !jsonString.equals("")) {
                    Gson gson = new Gson();
                    Menu menu = gson.fromJson(jsonString, Menu.class);
                    if (menu != null) {
                        ArrayList<MenuContents> menuContentsArrayList = menu
                                .getMenuContentsArrayList();
                        if (menuContentsArrayList != null && menuContentsArrayList.size() > 0) {
                            getAllMenuArrayUpdate(menuContentsArrayList);
                        }

                    }

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void getAllMenuArrayUpdate(ArrayList<MenuContents> menuContentsArrayList) {
        for (int i = 0; i < menuContentsArrayList.size(); i++) {
            MenuContents menuContents = menuContentsArrayList.get(i);
            if (menuContents != null) {
                getJsonMenuElements(menuContents);
            }
        }
    }

    private void getJsonMenuElements(MenuContents menuContents) {
        String content = menuContents.getContent();
        List<String> dataList = null;
        int size;
        String menuID = menuContents.getMenuID();
        switch (menuID) {
            case NEWS_UPDATE:
                dataList = decodeStringList(content);
                size = NewsUpdate.getNewsUpdateList().size();
                if (dataList != null && dataList.size() >= 3) {
                    if (dataList.size() % 3 != 0) {
                        dataList.remove(dataList.size() - 1);
                    }
                    Log.e(getClass().getName(), "Total News is :" + String
                            .valueOf(dataList.size()));
                    updateNews(dataList);
                    updated = true;
                }
                break;
            case NOTICE_BOARD:
                dataList = decodeStringList(content);
                size = Notice.getNoticeList().size();
                if (dataList != null && dataList.size() >= 2) {
                    if (dataList.size() % 2 != 0) {
                        dataList.remove(dataList.size() - 1);
                    }
                    Log.e(getClass().getName(), "Total Notice is :" + String
                            .valueOf(dataList.size()));
                    updateNotice(dataList);
                    updated = true;
                }
                break;

        }
    }

    private void updateNotice(List<String> dataList) {
        List<Notice> noticeList = Notice.getNoticeList();
        int size = noticeList.size();
        String name = null;
        String urlLink = null;
        int j = 0;
        if (noticeList != null && noticeList.size() > 0) {
            for (int i = 0; i < dataList.size(); i += 2) {
                Log.e(getClass().getName(), "Checking for update");
                name = dataList.get(i);
                urlLink = dataList.get(i + 1);
                if (i > size * 2) {
                    Notice notice = new Notice(name, urlLink);
                    Log.e(getClass().getName(), "Notice item added");
                } else {
                    j++;
                    Notice notice = Notice.load(Notice.class, j);
                    if (notice != null) {
                        if (notice.getRulsName() != null &&
                                !notice
                                        .getRulsName()
                                        .trim()
                                        .equals(name)) {
                            notice.setRulsName(name);
                            Log.e(getClass().getName(), "Updated Notice List");
                        }
                        if (notice.getUrlLink() != null &&
                                !notice
                                        .getUrlLink()
                                        .trim()
                                        .equals(urlLink)) {
                            notice.setUrlLink(urlLink);
                            Log.e(getClass().getName(), "Updated Notice List");
                        }
                        notice.save();
                    }
                }
            }
        }
    }

    private void updateNews(List<String> dataList) {
        List<NewsUpdate> newsUpdateList = NewsUpdate.getNewsUpdateList();
        int size = newsUpdateList.size();
        String headLine = null;
        String date = null;
        String details = null;
        int j = 0;
        if (newsUpdateList != null && newsUpdateList.size() > 0) {
            for (int i = 0; i < dataList.size(); i += 3) {
                Log.e(getClass().getName(), "Checking for update");
                headLine = dataList.get(i);
                date = dataList.get(i + 1);
                details = dataList.get(i + 2);
                if (i > size * 3) {
                    NewsUpdate newsUpdate = new NewsUpdate(headLine, date, details);
                    Log.e(getClass().getName(), "News Update item added");
                } else {
                    j++;
                    NewsUpdate newsUpdate = NewsUpdate.load(NewsUpdate.class, j);
                    if (newsUpdate.getHeadline() != null &&
                            !newsUpdate
                                    .getHeadline()
                                    .trim()
                                    .equals(headLine)) {
                        newsUpdate.setHeadline(headLine);
                        Log.e(getClass().getName(), "Updated News List");
                    }
                    if (newsUpdate.getDate() != null &&
                            !newsUpdate
                                    .getDate()
                                    .trim()
                                    .equals(date)) {
                        newsUpdate.setDate(date);
                        Log.e(getClass().getName(), "Updated News List");
                    }
                    if (newsUpdate.getDetails() != null &&
                            !newsUpdate
                                    .getDetails()
                                    .trim()
                                    .equals(details)) {
                        newsUpdate.setDetails(details);
                        Log.e(getClass().getName(), "Updated News List");
                    }
                    newsUpdate.save();

                }
            }
        }
    }


}